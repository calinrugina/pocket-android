package com.sqnp.pmm.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

@DatabaseTable(tableName = "reminders")
public class Reminder implements Account.AccountAwareEntity, Serializable {

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_ACCOUNT_ID = "account_id";
    public static final String COLUMN_VALUE = "value";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_CURRENCY = "currency";
    public static final String COLUMN_NEXT_DATE = "next_date";
    public static final String COLUMN_CREATED_AT = "created_at";

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    protected Integer id;

    @DatabaseField(columnName = COLUMN_TITLE)
    protected String title;

    @DatabaseField(columnName = COLUMN_ACCOUNT_ID)
    protected Integer accountId;

    @DatabaseField(columnName = COLUMN_VALUE)
    protected Double value;

    @DatabaseField(columnName = COLUMN_TYPE)
    protected Integer type;

    @DatabaseField(columnName = COLUMN_CURRENCY)
    protected String currency;

    @DatabaseField(columnName = COLUMN_NEXT_DATE, dataType = DataType.DATE_LONG)
    protected Date nextDate;

    @DatabaseField(columnName = COLUMN_CREATED_AT, dataType = DataType.DATE_LONG)
    protected Date createdAt;

    protected Account account;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getNextDate() {
        return nextDate;
    }

    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Account getAccount() {
        return account;
    }

    @Override
    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean isOfType(int type) {
        return null != this.type && this.type.equals(type);
    }
}
