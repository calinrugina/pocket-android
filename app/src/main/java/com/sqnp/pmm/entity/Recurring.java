package com.sqnp.pmm.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "accrecc")
public class Recurring {

    public static final int TYPE_DEFAULT = 0;
    public static final int TYPE_REMINDER = 1;

    public static final String COLUMN_ID = "mId";
    public static final String COLUMN_DESCRIPTION = "mDesc";
    public static final String COLUMN_ACCOUNT_ID = "mAccount";
    public static final String COLUMN_PAY_DAY = "mPayDay";
    public static final String COLUMN_VALUE = "mValue";
    public static final String COLUMN_NEXT_DATE = "mNextDate";
    public static final String COLUMN_LAST_DATE = "mLastDate";
    public static final String COLUMN_ACTIVE = "mActive";

    public static final String COLUMN_FREQUENCY = "frequency";
    public static final String COLUMN_RECURRING_FREQUENCY = "recurring_frequency";
    public static final String COLUMN_TYPE = "type";

    public static final int RECURRING_FREQUENCY_DAY = 1;
    public static final int RECURRING_FREQUENCY_WEEK = 2;
    public static final int RECURRING_FREQUENCY_MONTH = 3;
    public static final int RECURRING_FREQUENCY_YEAR = 4;

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    protected Integer id;

    @DatabaseField(columnName = COLUMN_DESCRIPTION)
    protected String description;

    @DatabaseField(columnName = COLUMN_ACCOUNT_ID)
    protected Integer accountId;

    @DatabaseField(columnName = COLUMN_VALUE)
    protected Double value;

    @DatabaseField(columnName = COLUMN_PAY_DAY, canBeNull = true)
    protected Integer payDay;

    @DatabaseField(columnName = COLUMN_NEXT_DATE, canBeNull = true, dataType = DataType.DATE_LONG)
    protected Date nextDate;

    @DatabaseField(columnName = COLUMN_LAST_DATE, canBeNull = true, dataType = DataType.DATE_LONG)
    protected Date lastDate;

    @DatabaseField(columnName = COLUMN_ACTIVE)
    protected boolean active;

    @DatabaseField(columnName = COLUMN_FREQUENCY, canBeNull = true)
    protected Integer frequency;

    @DatabaseField(columnName = COLUMN_RECURRING_FREQUENCY, canBeNull = true)
    protected Integer recurringFrequency;

    @DatabaseField(columnName = COLUMN_TYPE)
    protected Integer type = TYPE_DEFAULT;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getPayDay() {
        return payDay;
    }

    public void setPayDay(Integer payDay) {
        this.payDay = payDay;
    }

    public Date getNextDate() {
        return nextDate;
    }

    public void setNextDate(Date nextDate) {
        this.nextDate = nextDate;
    }

    public Date getLastDate() {
        return lastDate;
    }

    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public Integer getRecurringFrequency() {
        return recurringFrequency;
    }

    public void setRecurringFrequency(Integer recurringFrequency) {
        this.recurringFrequency = recurringFrequency;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
