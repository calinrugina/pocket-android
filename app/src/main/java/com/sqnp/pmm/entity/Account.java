package com.sqnp.pmm.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

@DatabaseTable(tableName = "account")
public class Account implements Serializable {

    public static interface AccountAwareEntity {

        public Integer getAccountId();
        public void setAccount(Account account);
        public Account getAccount();
    }

    public static final String COLUMN_ID = "mId";
    public static final String COLUMN_NAME = "mName";
    public static final String COLUMN_CREATED_AT = "mCDate";
    public static final String COLUMN_CURRENCY = "mCurrency";
    public static final String COLUMN_MONEY = "mMoney";
    public static final String COLUMN_RECURRING_ID = "mRecurringId";

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    protected Integer id;

    @DatabaseField(columnName = COLUMN_NAME)
    protected String name;

    @DatabaseField(columnName = COLUMN_CREATED_AT, dataType = DataType.DATE_LONG)
    protected Date createdAt;

    @DatabaseField(columnName = COLUMN_CURRENCY)
    protected String currency;

    @DatabaseField(columnName = COLUMN_MONEY)
    protected Double money;

    @DatabaseField(columnName = COLUMN_RECURRING_ID)
    protected Integer recurringId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Integer getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(Integer recurringId) {
        this.recurringId = recurringId;
    }
}
