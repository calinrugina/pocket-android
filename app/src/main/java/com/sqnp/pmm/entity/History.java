package com.sqnp.pmm.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

@DatabaseTable(tableName = "acchistory")
public class History implements Account.AccountAwareEntity, Serializable {

    public static final int TYPE_DEBIT = 1;
    public static final int TYPE_CREDIT = 2;
    public static final int TYPE_RECURRING = 3;

    public static final String COLUMN_ID = "mId";
    public static final String COLUMN_TITLE = "mTitle";
    public static final String COLUMN_ACCOUNT_ID = "mAccount";
    public static final String COLUMN_VALUE = "mValue";
    public static final String COLUMN_BALANCE = "mBalance";
    public static final String COLUMN_CURRENCY = "mCurrency";
    public static final String COLUMN_CREATED_AT = "mCDate";
    public static final String COLUMN_TYPE = "mType";

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    protected Integer id;

    @DatabaseField(columnName = COLUMN_TITLE)
    protected String title;

    @DatabaseField(columnName = COLUMN_ACCOUNT_ID)
    protected Integer accountId;

    @DatabaseField(columnName = COLUMN_VALUE)
    protected Double value;

    @DatabaseField(columnName = COLUMN_BALANCE)
    protected Double balance;

    @DatabaseField(columnName = COLUMN_CURRENCY)
    protected String currency;

    @DatabaseField(columnName = COLUMN_CREATED_AT, dataType = DataType.DATE_LONG)
    protected Date createdAt;

    @DatabaseField(columnName = COLUMN_TYPE)
    protected Integer type;

    protected Account account;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public Account getAccount() {
        return account;
    }

    @Override
    public void setAccount(Account account) {
        this.account = account;
    }

    public boolean isOfType(int type) {
        return null != this.type && this.type.equals(type);
    }
}
