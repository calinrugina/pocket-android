package com.sqnp.pmm.service;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.sqnp.pmm.R;
import com.sqnp.pmm.SharedPrefs_;
import com.sqnp.pmm.content.feature.ActivityResultHandler;
import com.sqnp.pmm.content.feature.BackgroundObserver;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EBean
public class PaymentService implements ActivityResultHandler {

    public static final int GOOGLE_INTENT_REQUEST_CODE = 1232;

    public static final int GOOGLE_BILLING_RESULT_UNDEFINED = -1;

    public static final int GOOGLE_BILLING_RESULT_OK = 0;
    public static final int GOOGLE_BILLING_RESULT_USER_CANCELED = 1;
    public static final int GOOGLE_BILLING_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int GOOGLE_BILLING_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int GOOGLE_BILLING_RESULT_DEVELOPER_ERROR = 5;
    public static final int GOOGLE_BILLING_RESULT_ERROR = 6;
    public static final int GOOGLE_BILLING_RESULT_ITEM_ALREADY_OWNED = 7;
    public static final int GOOGLE_BILLING_RESULT_ITEM_NOT_OWNED = 8;

    @RootContext
    protected Context context;

    @Pref
    protected SharedPrefs_ sharedPrefs;

    protected IInAppBillingService googleBillingService;

    protected BackgroundObserver pendingObserver;
    protected Exception error;

    public void setGoogleBillingService(IInAppBillingService googleBillingService) {
        this.googleBillingService = googleBillingService;
    }

    public boolean hasError() {
        return null != error;
    }

    public Exception getError() {
        return error;
    }

    public void checkForAppVersion(@NonNull BackgroundObserver observer) {
        reset();
        observer.onBackgroundWillStart();
        doRetrieveGooglePurchasedItems(observer);
    }

    public void purchasePaidVersion(@NonNull BackgroundObserver first, @NonNull BackgroundObserver second) {
        reset();

        first.onBackgroundWillStart();

        if (sharedPrefs.version().get() == context.getResources().getInteger(R.integer.app_version_paid)) {
            second.onBackgroundEnded();
            return;
        }

        doRetrieveGoogleSKUDetails(first, second);
    }

    private void reset() {
        error = null;
        pendingObserver = null;
    }

    @Background
    protected void doRetrieveGooglePurchasedItems(BackgroundObserver observer) {
        try {
            onGooglePurchasedItemsRetrieved(googleBillingService.getPurchases(
                    context.getResources().getInteger(R.integer.google_billing_version),
                    context.getPackageName(),
                    context.getString(R.string.google_billing_category_product),
                    null), observer);
        } catch (RemoteException e) {
            e.printStackTrace();
            onGooglePurchasedItemsRetrieved(null, observer);
        }
    }

    @UiThread
    protected void onGooglePurchasedItemsRetrieved(Bundle response, BackgroundObserver observer) {
        try {
            List<String> purchases = response.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
            if (null == purchases || purchases.isEmpty()) {
                throw new Exception();
            }

            sharedPrefs.version().put(context.getResources().getInteger(R.integer.app_version_paid));
        } catch (Exception e) {
            sharedPrefs.version().put(context.getResources().getInteger(R.integer.app_version_free));
        }

        observer.onBackgroundEnded();
    }

    @Background
    protected void doRetrieveGoogleSKUDetails(BackgroundObserver first, BackgroundObserver second) {
        Bundle details = new Bundle();
        details.putStringArrayList("ITEM_ID_LIST", new ArrayList<String>() {{
            add(context.getString(R.string.google_billing_product_paid_version));
        }});

        try {
            onGoogleSKUDetailsRetrieved(
                    googleBillingService.getSkuDetails(
                            context.getResources().getInteger(R.integer.google_billing_version),
                            context.getPackageName(),
                            context.getString(R.string.google_billing_category_product),
                            details), first, second);
        } catch (Exception e) {
            e.printStackTrace();
            onGoogleSKUDetailsRetrieved(null, first, second);
        }
    }

    @UiThread
    protected void onGoogleSKUDetailsRetrieved(Bundle bundle, BackgroundObserver first, BackgroundObserver second) {
        if (null == bundle
                || GOOGLE_BILLING_RESULT_OK != bundle.getInt("RESPONSE_CODE",
                GOOGLE_BILLING_RESULT_UNDEFINED)) {
            Log.e("GOOGLE", String.valueOf(bundle));
            error = new Exception();
            first.onBackgroundEnded();
            return;
        }

        List<String> products = bundle.getStringArrayList("DETAILS_LIST");
        if (null == products || 1 != products.size()) {
            Log.e("GOOGLE", String.valueOf(products));
            error = new Exception();
            first.onBackgroundEnded();
            return;
        }

        doRetrieveGoogleBuyIntent(first, second);
    }

    @Background
    protected void doRetrieveGoogleBuyIntent(BackgroundObserver first, BackgroundObserver second) {
        try {
            Bundle result = googleBillingService.getBuyIntent(
                    context.getResources().getInteger(R.integer.google_billing_version),
                    context.getPackageName(),
                    context.getString(R.string.google_billing_product_paid_version),
                    context.getString(R.string.google_billing_category_product),
                    String.format("goo.gl_%d", new Date().getTime()));

            if (null != result
                    && GOOGLE_BILLING_RESULT_OK == result.getInt("RESPONSE_CODE",
                    GOOGLE_BILLING_RESULT_UNDEFINED)) {
                onGoogleBuyIntentRetrieved((PendingIntent) result.getParcelable("BUY_INTENT"), first, second);
                return;
            }

            onGoogleBuyIntentRetrieved(null, first, second);
        } catch (Exception e) {
            onGoogleBuyIntentRetrieved(null, first, second);
        }
    }

    @UiThread
    protected void onGoogleBuyIntentRetrieved(PendingIntent buyIntent, BackgroundObserver first, BackgroundObserver second) {
        if (null == buyIntent) {
            error = new Exception();
            first.onBackgroundEnded();
            return;
        }

        try {
            ((Activity) context).startIntentSenderForResult(
                    buyIntent.getIntentSender(),
                    GOOGLE_INTENT_REQUEST_CODE,
                    new Intent(),
                    0, 0, 0);
            pendingObserver = second;
            first.onBackgroundEnded();
        } catch (Exception e) {
            error = e;
            first.onBackgroundEnded();
        }
    }

    @Override
    public boolean handleActivityResult(Activity context, int requestCode, int resultCode, Intent data) {
        if (requestCode == GOOGLE_INTENT_REQUEST_CODE || null == pendingObserver) {
            return false;
        }

        checkForAppVersion(pendingObserver);

        return true;
    }
}
