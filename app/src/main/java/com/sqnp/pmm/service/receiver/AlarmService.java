package com.sqnp.pmm.service.receiver;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sqnp.pmm.R;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.Reminder;
import com.sqnp.pmm.service.db.DatabaseHelper;

import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.annotations.SystemService;

@EIntentService
public class AlarmService extends IntentService {

    public static final String ACTION_PUBLISH_NOTIFICATION = "publishNotification";

    @OrmLiteDao(helper = DatabaseHelper.class, model = Reminder.class)
    protected RuntimeExceptionDao<Reminder, Integer> remindersRepository;

    @OrmLiteDao(helper = DatabaseHelper.class, model = Account.class)
    protected RuntimeExceptionDao<Account, Integer> accountsRepository;

    @SystemService
    protected NotificationManager notificationManager;

    public AlarmService() {
        super("AlarmService");
    }

    @ServiceAction(ACTION_PUBLISH_NOTIFICATION)
    protected void publishNotificationAction(Integer reminderId) {
        Reminder reminder = remindersRepository.queryForId(reminderId);
        if (null == reminder || null == reminder.getAccountId()) {
            return;
        }

        reminder.setAccount(accountsRepository.queryForId(reminder.getAccountId()));
        if (null == reminder.getAccount()) {
            return;
        }

        // Create a pending intent to open the the application when the notification is clicked.
        //Restart the app.
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(
                getBaseContext().getPackageName());

        String msg = reminder.getTitle();
        if (null == msg || null == launchIntent) {
            return;
        }

        launchIntent.putExtra(MainActivity.EXTRA_REMINDER_ID, reminderId);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, -1, launchIntent,
                0);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent);

        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(reminderId, notification);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }
}
