package com.sqnp.pmm.service.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.entity.Recurring;
import com.sqnp.pmm.entity.Reminder;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    protected static final String DATABASE_NAME = "pocket_mm_db";
    protected static final int DATABASE_VERSION = 2;

    protected Dao<Account, Integer> accountsDao;
    private RuntimeExceptionDao<Account, Integer> accountsRuntimeDao;

    protected Dao<Recurring, Integer> recurringDao;
    private RuntimeExceptionDao<Recurring, Integer> recurringRuntimeDao;

    protected Dao<History, Integer> historyDao;
    private RuntimeExceptionDao<History, Integer> historyRuntimeDao;

    protected Dao<Reminder, Integer> remindersDao;
    private RuntimeExceptionDao<Reminder, Integer> remindersRuntimeDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public Dao<Account, Integer> getAccountsDao() throws SQLException {
        if (null == accountsDao) {
            accountsDao = getDao(Account.class);
        }

        return accountsDao;
    }

    public RuntimeExceptionDao<Account, Integer> getAccountsRuntimeDao() {
        if (null == accountsRuntimeDao) {
            accountsRuntimeDao = getRuntimeExceptionDao(Account.class);
        }

        return accountsRuntimeDao;
    }

    public Dao<Reminder, Integer> getRemindersDao() throws SQLException {
        if (null == remindersDao) {
            remindersDao = getDao(Reminder.class);
        }

        return remindersDao;
    }

    public RuntimeExceptionDao<Reminder, Integer> getRemindersRuntimeDao() {
        if (null == remindersRuntimeDao) {
            remindersRuntimeDao = getRuntimeExceptionDao(Reminder.class);
        }

        return remindersRuntimeDao;
    }

    public Dao<Recurring, Integer> getRecurringDao() throws SQLException {
        if (null == recurringDao) {
            recurringDao = getDao(Recurring.class);
        }

        return recurringDao;
    }

    public RuntimeExceptionDao<Recurring, Integer> getRecurringRuntimeDao() {
        if (null == recurringRuntimeDao) {
            recurringRuntimeDao = getRuntimeExceptionDao(Recurring.class);
        }

        return recurringRuntimeDao;
    }

    public Dao<History, Integer> getHistorysDao() throws SQLException {
        if (null == historyDao) {
            historyDao = getDao(History.class);
        }

        return historyDao;
    }

    public RuntimeExceptionDao<History, Integer> getHistoryRuntimeDao() {
        if (null == historyRuntimeDao) {
            historyRuntimeDao = getRuntimeExceptionDao(History.class);
        }

        return historyRuntimeDao;
    }

    protected void migrateFromFirstVersion() throws SQLException {
        Dao<Recurring, Integer> recurringDao = getRecurringDao();
        if (recurringDao.isTableExists()) {
            String stmt = "ALTER TABLE `%s` ADD COLUMN `%s` %s";
            recurringDao.executeRawNoArgs(String.format(stmt, "accrecc", Recurring.COLUMN_FREQUENCY, "INTEGER NULL"));
            recurringDao.executeRawNoArgs(String.format(stmt, "accrecc", Recurring.COLUMN_RECURRING_FREQUENCY, "INTEGER NULL"));
            recurringDao.executeRawNoArgs(String.format(stmt, "accrecc", Recurring.COLUMN_TYPE, "INTEGER DEFAULT " + Recurring.TYPE_DEFAULT));

            recurringDao.executeRawNoArgs(String.format(
                    "UPDATE `%s` SET `%s` = %d, `%s` = 1",
                    "accrecc",
                    Recurring.COLUMN_RECURRING_FREQUENCY,
                    Recurring.RECURRING_FREQUENCY_WEEK,
                    Recurring.COLUMN_FREQUENCY));
        }

        Dao<History, Integer> historyDao = getHistorysDao();
        if (historyDao.isTableExists()) {
            historyDao.executeRawNoArgs(String.format(
                    "UPDATE `%s` SET `%s` = CASE `%s` WHEN %d THEN %d ELSE %d END",
                    "acchistory",
                    History.COLUMN_TYPE, History.COLUMN_TYPE,
                    History.TYPE_CREDIT, History.TYPE_DEBIT,
                    History.TYPE_CREDIT));

            String stmt = "UPDATE `%s` SET `%s` = `%s` %s `%s` WHERE `%s` = %d";
            historyDao.executeRawNoArgs(String.format(
                    stmt, "acchistory", History.COLUMN_BALANCE, History.COLUMN_BALANCE,
                    "+", History.COLUMN_VALUE, History.COLUMN_TYPE, History.TYPE_DEBIT));
            historyDao.executeRawNoArgs(String.format(
                    stmt, "acchistory", History.COLUMN_BALANCE, History.COLUMN_BALANCE,
                    "-", History.COLUMN_VALUE, History.COLUMN_TYPE, History.TYPE_CREDIT));
        }

        TableUtils.createTable(connectionSource, Reminder.class);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        Log.e("ON_CREATE", "here");
        try {
            if (getAccountsDao().isTableExists()
                    && getRecurringDao().isTableExists()
                    && getHistorysDao().isTableExists()) {

                Log.e("ON_CREATE", "upgrade from version 1");
                migrateFromFirstVersion();
            } else {
                TableUtils.createTable(connectionSource, Account.class);
                TableUtils.createTable(connectionSource, Recurring.class);
                TableUtils.createTable(connectionSource, History.class);
                TableUtils.createTable(connectionSource, Reminder.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    
    @Override
    public void onUpgrade(
            SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        Log.e("ON_UPGRADE", String.valueOf(i));
        try {
            if (i < 2) {
                Log.e("ON_UPGRADE", "from version 1");
                migrateFromFirstVersion();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        super.close();

        accountsDao = null;
        accountsRuntimeDao = null;

        recurringDao = null;
        recurringRuntimeDao = null;

        historyDao = null;
        historyRuntimeDao = null;

        remindersDao = null;
        remindersRuntimeDao = null;

    }

}
