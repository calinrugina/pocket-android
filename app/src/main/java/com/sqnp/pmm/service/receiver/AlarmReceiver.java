package com.sqnp.pmm.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.ReceiverAction;

@EReceiver
public class AlarmReceiver extends BroadcastReceiver {

    public static final String ACTION_NOTIFICATION = "notification";

    @ReceiverAction(ACTION_NOTIFICATION)
    protected void notificationAction(@ReceiverAction.Extra int reminderId, Context context) {
        AlarmService_.intent(context)
                .publishNotificationAction(reminderId)
                .start();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
    }
}
