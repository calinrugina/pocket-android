package com.sqnp.pmm.support;

import android.os.Environment;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.entity.Recurring;
import com.sqnp.pmm.entity.Reminder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

public final class Utils {

    public static List<Currency> getAvailableCurrencies() {
        return getAvailableCurrencies(null);
    }

    public static List<Currency> getAvailableCurrencies(String defaultCurrency) {
        List<Currency> currencies = new ArrayList<>();
        for (Locale locale: Locale.getAvailableLocales()) {
            Currency currency;
            try {
                currency = Currency.getInstance(locale);
            } catch (Exception e) {
                currency = null;
            }

            if (null == currency || currencies.contains(currency)) {
                continue;
            }

            currencies.add(currency);
        }

        Currency fallback;
        try {
            if (null != defaultCurrency) {
                fallback = Currency.getInstance(defaultCurrency);
            } else {
                fallback = Currency.getInstance(Locale.getDefault());
            }
        } catch (Exception e) {
            fallback = Currency.getInstance(Locale.getDefault());
        }

        if (null != fallback && currencies.contains(fallback)) {
            currencies.remove(fallback);
        }

        Collections.sort(currencies, new Comparator<Currency>() {

            @Override
            public int compare(Currency lhs, Currency rhs) {
                return lhs.toString().compareTo(rhs.toString());
            }
        });
        if (null != fallback) {
            currencies.add(0, fallback);
        }

        return currencies;
    }

    public static String formatBalance(double value, String currencyCode) {
        return formatBalance(value, Currency.getInstance(currencyCode));
    }

    public static String formatBalance(double value, Currency currency) {
        DecimalFormat format = new  DecimalFormat("¤ 0.##");

        try {
            format.setCurrency(currency);
        } catch (IndexOutOfBoundsException e) {
            format.setCurrency(getAvailableCurrencies().get(0));
        }

        return format.format(value);
    }

    public static boolean emptyDatabase(ConnectionSource connectionSource) {
        try {
            TableUtils.dropTable(connectionSource, History.class, true);
            TableUtils.dropTable(connectionSource, Recurring.class, true);
            TableUtils.dropTable(connectionSource, Reminder.class, true);
            TableUtils.dropTable(connectionSource, Account.class, true);

            TableUtils.createTable(connectionSource, History.class);
            TableUtils.createTable(connectionSource, Recurring.class);
            TableUtils.createTable(connectionSource, Reminder.class);
            TableUtils.createTable(connectionSource, Account.class);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static File exportDatabase(ConnectionSource connectionSource) {

        File data = Environment.getDataDirectory();
        String currentDBPath = "/data/"+ "com.sqnp.pmm" +"/databases/"+"pocket_mm_db";
        File file = new File(data, currentDBPath);

        try {
            FileInputStream fis = new FileInputStream(file);

            SimpleDateFormat mFormatter = new SimpleDateFormat("yyyyMMddHHmmss");
            Calendar c = Calendar.getInstance();
            String outFileName = Environment.getExternalStorageDirectory()+"/pocket_"+mFormatter.format(c.getTime())+"_db.pkta";
            OutputStream output = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer))>0){
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            fis.close();

            File backupFile = new File(outFileName);

            return backupFile;
        } catch (Exception e) {
            return file;
        }
    }
}
