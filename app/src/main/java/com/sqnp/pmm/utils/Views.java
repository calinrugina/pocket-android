package com.sqnp.pmm.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.sqnp.pmm.R;

import java.util.HashMap;
import java.util.Map;

public abstract class Views {

    protected static Map<String, Typeface> typeFaces;

    protected static Typeface getTypeface(String resource, Context context) {
        if (null == typeFaces) {
            typeFaces = new HashMap<String, Typeface>();
        }

        if (! typeFaces.containsKey(resource)) {
            typeFaces.put(
                    resource,
                    Typeface.createFromAsset(
                            context.getAssets(),
                            resource));
        }

        return typeFaces.get(resource);
    }

    public static void initWithAttributes(TextView view, AttributeSet attrs) {
        if (null == view || null == view.getContext()) {
            return;
        }

        Context context = view.getContext();
        TypedArray a = context.obtainStyledAttributes(
                attrs,
                R.styleable.FontFaceAware
        );

        if (null == a) {
            return;
        }

        if (! a.hasValue(R.styleable.FontFaceAware_fontFace)) {
            return;
        }

        try {
            view.setTypeface(getTypeface(a.getString(R.styleable.FontFaceAware_fontFace), context));
            view.setPaintFlags(view.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        } catch (Exception e) {}

        a.recycle();
    }
}
