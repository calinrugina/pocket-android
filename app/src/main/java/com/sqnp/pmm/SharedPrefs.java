package com.sqnp.pmm;

import org.androidannotations.annotations.sharedpreferences.DefaultRes;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface SharedPrefs {

    public String currency();

    @DefaultRes(R.integer.app_version_free)
    public int version();
}
