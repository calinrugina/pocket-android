package com.sqnp.pmm.content.feature;

public interface BackgroundObserver {

    public void onBackgroundWillStart();
    public void onBackgroundEnded();
}
