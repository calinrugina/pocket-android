package com.sqnp.pmm.content.feature;

import android.app.Activity;
import android.content.Intent;

public interface ActivityResultHandler {

    public boolean handleActivityResult(Activity context, int requestCode, int resultCode, Intent data);
}
