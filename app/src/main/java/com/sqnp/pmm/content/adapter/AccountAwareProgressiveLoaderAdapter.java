package com.sqnp.pmm.content.adapter;

import android.util.SparseArray;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.sqnp.pmm.entity.Account;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.List;

@EBean
public abstract class AccountAwareProgressiveLoaderAdapter<T extends Account.AccountAwareEntity> extends
        AbstractProgressiveLoaderAdapter<T> {

    private SparseArray<Account> accounts;
    private RuntimeExceptionDao<Account, Integer> accountsRepository;

    public void setAccountsRepository(RuntimeExceptionDao<Account, Integer> accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    protected void onLoaded(List<T> data) {
        if (null != data) {
            doLoadMissingAccounts(data);
        } else {
            super.onLoaded(data);
        }
    }

    @Background
    protected void doLoadMissingAccounts(List<T> data) {
        if (null == accounts) {
            accounts = new SparseArray<>();
        }

        List<Integer> missingAccounts = new ArrayList<>();
        boolean exitEarly = true;
        for (T accountAware : data) {
            if (null == accountAware.getAccountId()) {
                continue;
            }

            Account account = accounts.get(accountAware.getAccountId());
            if (null != account) {
                accountAware.setAccount(account);
            }

            missingAccounts.add(accountAware.getAccountId());
            exitEarly = false;
        }

        if (exitEarly) {
            onMissingAccountsLoaded(data);
            return;
        }

        List<Account> partialAccounts;
        QueryBuilder<Account, Integer> accountsQueryBuilder = accountsRepository.queryBuilder();
        try {
            accountsQueryBuilder.where()
                    .in(Account.COLUMN_ID, missingAccounts);

            partialAccounts = accountsRepository.query(accountsQueryBuilder.prepare());
            if (null == partialAccounts) {
                throw new Exception();
            }
        } catch (Exception e) {
            onMissingAccountsLoaded(data);
            return;
        }

        for (Account account : partialAccounts) {
            accounts.put(account.getId(), account);
        }

        for (T accountAware : data) {
            if (null == accountAware.getAccountId() || null != accountAware.getAccount()) {
                continue;
            }

            accountAware.setAccount(accounts.get(accountAware.getAccountId()));
        }

        onMissingAccountsLoaded(data);
    }

    @UiThread
    protected void onMissingAccountsLoaded(List<T> data) {
        super.onLoaded(data);
    }
}
