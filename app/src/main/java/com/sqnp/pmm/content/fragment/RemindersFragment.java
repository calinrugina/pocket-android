package com.sqnp.pmm.content.fragment;

import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.BaseFragmentActivity;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.adapter.RemindersAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.content.feature.ViewPrototypeBuilder;
import com.sqnp.pmm.content.view.ReminderItemView;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.Reminder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

@EFragment(R.layout.fragment_reminders)
public class RemindersFragment extends BaseFragment implements
        SectionTitleProvider,
        ReminderItemView.TransactionClickedListener,
        ReminderItemView.RemoveClickedListener {

    @FragmentArg
    protected Account account;

    @ViewById(R.id.reminders_reminders_list)
    protected StickyListHeadersListView remindersList;

    @Bean
    protected RemindersAdapter reminders;

    @AfterViews
    protected void configureViews() {
        reminders.setRemindersRepository(((MainActivity) getActivity()).getRemindersRepository());
        reminders.setAccountsRepository(((MainActivity) getActivity()).getAccountsRepository());
        reminders.setViewPrototypeBuilder(new ViewPrototypeBuilder<ReminderItemView>() {
            
            @Override
            public ReminderItemView buildViewPrototype(ReminderItemView view) {
                view.setTransactionClickedListener(RemindersFragment.this);
                view.setRemoveClickedListener(RemindersFragment.this);

                return view;
            }
        });
        reminders.setAccount(account);

        remindersList.setAdapter(reminders);
        remindersList.setOnScrollListener(reminders);

        remindersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                createReminderTransaction((Reminder) parent.getItemAtPosition(position));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        reminders.reset();
        reminders.load();
    }

    @Override
    public Title getTitleHolder() {
        return new Title(getString(R.string.reminders_fragment_title),
                new Pair<Integer, OnMenuItemAttachedCallback>(R.drawable.ic_action_add, new OnMenuItemAttachedCallback() {

                    @Override
                    public void onMenuItemAttached(ImageView view) {
                        if (0  == ((MainActivity) getActivity()).getAccountsRepository().countOf()) {
                            view.setVisibility(View.INVISIBLE);
                        } else {
                            view.setVisibility(View.VISIBLE);
                            view.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    if (((MainActivity) getActivity()).isFreeVersion()
                                            && ((MainActivity) getActivity()).getRemindersRepository().countOf()
                                            >= getResources().getInteger(R.integer.free_version_max_reminders)) {

                                        if (((MainActivity) getActivity()).showFreeVersionRestrictionDialog() )
                                            ((BaseFragmentActivity) getActivity()).pushFragment("reminder");
                                        return;
                                    }

                                    ((BaseFragmentActivity) getActivity()).pushFragment("reminder");
                                }
                            });
                        }
                    }
                }));
    }

    @Override
    public void onRemoveReminderClicked(final Reminder reminder) {
        AlertDialog.build(getActivity(), new AlertDialog.Builder() {

            @Override
            public AlertDialog buildAlertDialog(AlertDialog dialog) {
                return dialog
                        .setText(getString(R.string.reminders_alert_remove))
                        .setPositiveButton(getString(R.string.reminders_alert_remove_positive), new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                ((MainActivity) getActivity()).getRemindersRepository().delete(reminder);

                                reminders.reset();
                                reminders.load();
                            }
                        })
                        .setNegativeButton(null, null);
            }
        }).show(getFragmentManager());
    }

    @Override
    public void onTransactionReminderClicked(Reminder reminder) {
        createReminderTransaction(reminder);
    }

    protected void createReminderTransaction(final Reminder reminder) {
        ((BaseFragmentActivity) getActivity()).pushFragment("reminder_transaction", new BaseFragmentActivity.FragmentBuilder() {

            @Override
            public void buildFragment(Bundle fragmentBundle) {
                fragmentBundle.putSerializable("reminder", reminder);
            }
        });
    }
}
