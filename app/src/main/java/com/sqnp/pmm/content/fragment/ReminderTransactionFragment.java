package com.sqnp.pmm.content.fragment;

import android.text.TextUtils;
import android.util.Pair;
import android.widget.Spinner;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.adapter.SimpleSpinnerAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.feature.EntityExtractor;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.entity.Reminder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EFragment(R.layout.fragment_reminder_transaction)
public class ReminderTransactionFragment extends BaseFragment implements SectionTitleProvider {

    @FragmentArg
    protected Reminder reminder;

    @ViewById(R.id.reminder_transaction_fragment_title)
    protected TextView fragmentTitle;

    @ViewById(R.id.reminder_transaction_title)
    protected TextView title;

    @ViewById(R.id.reminder_transaction_amount)
    protected TextView amount;

    @ViewById(R.id.reminder_transaction_type)
    protected Spinner type;

    @Bean
    protected SimpleSpinnerAdapter<Pair<Integer, String>> types;

    @AfterViews
    protected void configureViews() {
        List<Pair<Integer, String>> reminderTypes = new ArrayList<>();
        int i = 1;
        for (String reminderType : getResources().getStringArray(R.array.spinner_transaction_type)) {
            reminderTypes.add(new Pair<>(i++, reminderType));
        }

        types.setEntityExtractor(new EntityExtractor<Pair<Integer, String>, String>() {

            @Override
            public String extractEntity(Pair<Integer, String> entity) {
                return entity.second;
            }
        });
        types.populate(reminderTypes);
        type.setAdapter(types);

        title.setText(reminder.getTitle());
        amount.setText(String.valueOf(reminder.getValue()));
        for (int j=0; j<types.getCount(); j++) {
            if (reminder.isOfType(types.getItem(j).first)) {
                type.setSelection(j);
                break;
            }
        }

        doLoadAccount();
    }

    @Background
    protected void doLoadAccount() {
        reminder.setAccount(((MainActivity) getActivity()).getAccountsRepository().queryForId(
                reminder.getAccountId()));
        onAccountLoaded();
    }

    @UiThread
    protected void onAccountLoaded() {
        fragmentTitle.setText(getString(
                R.string.reminder_transaction_title, reminder.getAccount().getName()));
    }

    @Click(R.id.reminder_transaction_save)
    protected void saveClicked() {
        AlertDialog dialog = null;
        if (TextUtils.isEmpty(title.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.reminder_alert_no_title))
                            .setPositiveButton(null, null);
                }
            });
        } else if (TextUtils.isEmpty(amount.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.reminder_alert_no_amount))
                            .setPositiveButton(null, null);
                }
            });
        }

        if (null != dialog) {
            dialog.show(getFragmentManager());
            return;
        }

        doSave();
    }

    @Background
    protected void doSave() {
        History transaction = new History();
        transaction.setAccountId(reminder.getAccountId());
        transaction.setCreatedAt(new Date());
        transaction.setCurrency(reminder.getCurrency());
        transaction.setTitle(title.getText().toString());
        transaction.setValue(Double.valueOf(amount.getText().toString()));
        transaction.setBalance(reminder.getAccount().getMoney());
        transaction.setType(((Pair<Integer, String>) type.getSelectedItem()).first);

        Account account = ((MainActivity) getActivity()).getAccountsRepository().queryForId(reminder.getAccountId());
        account.setMoney(account.getMoney() + transaction.getValue() * (transaction.isOfType(History.TYPE_DEBIT) ? -1 : 1));

        ((MainActivity) getActivity()).getHistoryRepository().create(transaction);
        ((MainActivity) getActivity()).getRemindersRepository().delete(reminder);
        ((MainActivity) getActivity()).getAccountsRepository().update(account);

        onSaved();
    }

    @UiThread
    protected void onSaved() {
        if (null != getActivity()) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public Title getTitleHolder() {
        Title title = new Title(getString(R.string.reminder_transaction_fragment_title));
        title.setHidesMenu(true);

        return title;
    }
}
