package com.sqnp.pmm.content.fragment;

import android.support.v4.view.ViewPager;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.adapter.TutorialPagerAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_tutorial)
public class TutorialFragment extends BaseFragment implements
        MainActivity.FeatureRequestFullContentFrame {

    @ViewById(R.id.tutorial_pages_container)
    protected ViewPager pagesContainer;

    @ViewById(R.id.tutorial_page_indicator)
    protected CirclePageIndicator pageIndicator;

    @Bean
    protected TutorialPagerAdapter pages;

    @AfterViews
    protected void configureViews() {
        pagesContainer.setAdapter(pages);
        pageIndicator.setViewPager(pagesContainer);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Click(R.id.tutorial_skip)
    protected void skipClicked() {
        getActivity().onBackPressed();
    }
}
