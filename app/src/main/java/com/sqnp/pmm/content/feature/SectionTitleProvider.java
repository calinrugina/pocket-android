package com.sqnp.pmm.content.feature;

import android.util.Pair;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface SectionTitleProvider {

    public static interface OnMenuItemAttachedCallback {

        public void onMenuItemAttached(ImageView view);
    }

    public static class Title implements BindableViewHolder<TextView> {

        protected String title;
        protected List<Pair<Integer, OnMenuItemAttachedCallback>> menuItems;
        protected boolean hidesMenu = false;

        public Title(String title, Pair<Integer,
                        OnMenuItemAttachedCallback> ...menuItems) {
            this.title = title;

            if (null != menuItems) {
                this.menuItems = new ArrayList<>();
                Collections.addAll(this.menuItems, menuItems);
            }
        }

        public boolean isHidesMenu() {
            return hidesMenu;
        }

        public void setHidesMenu(boolean hidesMenu) {
            this.hidesMenu = hidesMenu;
        }

        public boolean hasMenuItems() {
            return null != menuItems && ! menuItems.isEmpty();
        }

        public List<Pair<Integer, OnMenuItemAttachedCallback>> getMenuItems() {
            return menuItems;
        }

        public Title addMenuItem(Integer resource, OnMenuItemAttachedCallback callback) {
            if (null == menuItems) {
                menuItems = new ArrayList<>();
            }

            menuItems.add(new Pair<>(resource, callback));
            return this;
        }

        @Override
        public void bind(TextView entity) {
            entity.setText(title);
            entity.setGravity(Gravity.CENTER_VERTICAL
                    | (isHidesMenu() ? Gravity.LEFT : Gravity.CENTER_HORIZONTAL));
        }
    }

    public Title getTitleHolder();
}
