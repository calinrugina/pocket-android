package com.sqnp.pmm.content.dialog;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TimePicker;

import com.sqnp.pmm.R;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateDialog extends AlertDialog {

    protected DatePicker datePicker;
    protected TimePicker timePicker;

    protected EditText editText;

    protected boolean useTime;

    protected Date minDate;

    protected int timePickerInterval = 5;

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
    public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public DateDialog(Context context) {
        super(context);
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public void setTimePickerInterval(int timePickerInterval) {
        this.timePickerInterval = timePickerInterval;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_date);

        datePicker = (DatePicker) findViewById(R.id.date_date_picker);
        timePicker = (TimePicker) findViewById(R.id.date_time_picker);

        findViewById(R.id.date_cancel).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        findViewById(R.id.date_choose).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onChooseClicked();
            }
        });

        if (Build.VERSION.SDK_INT >= 11) {
            if (null != minDate) {
                datePicker.setMinDate(minDate.getTime() - 1000);
            }
        }

        configureViews();
    }

    public void setUseTime(boolean useTime) {
        this.useTime = useTime;
    }

    @TargetApi(11)
    public void configureViews() {
        if (useTime) {
            timePicker.setVisibility(View.VISIBLE);
            try {
                Class<?> classForid = Class.forName("com.android.internal.R$id");
                Field field = classForid.getField("minute");
                NumberPicker minuteSpinner = (NumberPicker)timePicker.findViewById(field.getInt(null));
                minuteSpinner.setMinValue(0);
                minuteSpinner.setMaxValue((60/timePickerInterval)-1);
                List<String> displayedValues = new ArrayList<>();
                for(int i = 0; i < 60; i+=timePickerInterval){
                    displayedValues.add(String.format("%02d",i));
                }
                minuteSpinner.setDisplayedValues(displayedValues.toArray(new String[0]));
                Calendar cal = Calendar.getInstance();
                if(null != editText) {
                    try {
                        cal.setTime(DATE_TIME_FORMAT.parse(editText.getText().toString()));
                    } catch (ParseException e) {
                    }
                }
                int hour=cal.get(Calendar.HOUR_OF_DAY);
                int min=cal.get(Calendar.MINUTE);
                if(null == editText && min > (60-timePickerInterval)){
                    min = 0;
                    hour ++;
                    if(hour > 23){
                        hour = 0;
                        cal.add(Calendar.DATE, 1);
                    }
                }

                int year=cal.get(Calendar.YEAR);
                int month=cal.get(Calendar.MONTH);
                int day=cal.get(Calendar.DAY_OF_MONTH);

                datePicker.updateDate(year, month, day);
                timePicker.setCurrentHour(hour);
                timePicker.setCurrentMinute((int)Math.ceil((double) min / timePickerInterval));

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            timePicker.setVisibility(View.GONE);
            try {
                Calendar cal = Calendar.getInstance();
                if(null != editText) {
                    try {
                        cal.setTime(DATE_FORMAT.parse(editText.getText().toString()));
                    } catch (ParseException e) {}
                }

                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                datePicker.updateDate(year, month, day);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onChooseClicked() {
        if(null != editText){
            Calendar cal = Calendar.getInstance();

            cal.set(Calendar.YEAR, datePicker.getYear());
            cal.set(Calendar.MONTH, datePicker.getMonth());
            cal.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());

            if (useTime) {
                cal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                cal.set(Calendar.MINUTE, timePicker.getCurrentMinute()*timePickerInterval);
                editText.setText(DATE_TIME_FORMAT.format(cal.getTime()));

                try {
                    editText.setTag(DATE_TIME_FORMAT.parse(editText.getText().toString()));
                } catch (ParseException e) {}
            } else {
                editText.setText(DATE_FORMAT.format(cal.getTime()));

                try {
                    editText.setTag(DATE_FORMAT.parse(editText.getText().toString()));
                } catch (ParseException e) {}
            }

        }
        dismiss();
    }

    public void setMinDate(Date date) {
        minDate = date;
    }
}