package com.sqnp.pmm.content.fragment;

import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.feature.SectionTitleProvider;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FromHtml;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_help)
public class HelpFragment extends BaseFragment implements SectionTitleProvider {

    @ViewById(R.id.help_content)
    @FromHtml(R.string.help_content)
    protected TextView content;

    @Override
    public Title getTitleHolder() {
        Title title = new Title(getString(R.string.help_fragment_title));
        title.setHidesMenu(true);

        return title;
    }
}
