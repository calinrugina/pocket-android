package com.sqnp.pmm.content.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.BaseFragmentActivity;
import com.sqnp.pmm.content.fragment.pager.TutorialPageFragment;
import com.sqnp.pmm.content.fragment.pager.TutorialPageFragment_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

@EBean
public class TutorialPagerAdapter extends FragmentPagerAdapter {

    @RootContext
    protected Context context;

    protected String[] pages;

    public TutorialPagerAdapter(Context context) {
        super(((BaseFragmentActivity) context).getShownFragment().getChildFragmentManager());
    }

    @AfterInject
    protected void configureAdapter() {
        pages = context.getResources().getStringArray(R.array.tutorial_pages);
    }

    @Override
    public Fragment getItem(int position) {
        TutorialPageFragment fragment = new TutorialPageFragment_();
        fragment.bind(pages[position]);

        return fragment;
    }

    @Override
    public int getCount() {
        return null != pages ? pages.length : 0;
    }
}
