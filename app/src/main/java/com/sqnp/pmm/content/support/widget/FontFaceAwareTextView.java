package com.sqnp.pmm.content.support.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.sqnp.pmm.utils.Views;

public class FontFaceAwareTextView extends TextView {

    public FontFaceAwareTextView(Context context) {
        super(context);
    }

    public FontFaceAwareTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Views.initWithAttributes(this, attrs);
    }

    public FontFaceAwareTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Views.initWithAttributes(this, attrs);
    }
}
