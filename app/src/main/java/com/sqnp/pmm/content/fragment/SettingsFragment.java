package com.sqnp.pmm.content.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.Spinner;

import com.sqnp.pmm.BuildConfig;
import com.sqnp.pmm.R;
import com.sqnp.pmm.content.BaseFragmentActivity;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.adapter.SimpleSpinnerAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.feature.BackgroundObserver;
import com.sqnp.pmm.content.feature.EntityExtractor;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.support.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemSelect;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Currency;

@EFragment(R.layout.fragment_settings)
public class SettingsFragment extends BaseFragment implements
        SectionTitleProvider,
        MainActivity.FeatureRequestNoAds {

    private static final int REQUEST_RATE_APP = 36;
    private static final int REQUEST_SEND_EMAIL = 37;

    @ViewById(R.id.settings_currency)
    protected Spinner currency;

    @ViewById(R.id.settings_buy)
    protected View buy;

    @Bean
    protected SimpleSpinnerAdapter<Currency> currencies;

    @AfterViews
    protected void configureViews() {
        currencies.setEntityExtractor(new EntityExtractor<Currency, String>() {

            @Override
            public String extractEntity(Currency entity) {
                return entity.toString();
            }
        });
        currencies.populate(Utils.getAvailableCurrencies(
                ((MainActivity) getActivity()).getSharedPrefs().currency().get()));
        currency.setAdapter(currencies);
    }

    @ItemSelect(R.id.settings_currency)
    protected void currencyItemSelected(boolean selected, Currency currency) {
        if (! selected) {
            return;
        }

        ((MainActivity) getActivity()).getSharedPrefs().currency().put(currency.getCurrencyCode());
    }

    @Click(R.id.settings_help)
    protected void helpClicked() {
        ((BaseFragmentActivity) getActivity()).pushFragment("help");
    }

    @Click(R.id.settings_tutorial)
    protected void tutorialClicked() {
        ((BaseFragmentActivity) getActivity()).pushFragment("tutorial", false);
    }

    @Click(R.id.settings_clear)
    protected void clearClicked() {
        AlertDialog.build(getActivity(), new AlertDialog.Builder() {

            @Override
            public AlertDialog buildAlertDialog(AlertDialog dialog) {
                return dialog
                        .setText(getString(R.string.settings_alert_clear))
                        .setPositiveButton(getString(R.string.settings_alert_clear_positive), new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                doClearDatabase();
                            }
                        })
                        .setNegativeButton(null, null);
            }
        }).show(getFragmentManager());
    }


    @Click(R.id.settings_export)
    protected void exportClicked() {


        SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd @ HH:mm");
        Calendar c = Calendar.getInstance();

        File databaseFile = Utils.exportDatabase(
                ((MainActivity) getActivity()).getAccountsRepository().getConnectionSource());

        Intent sendIntent = new Intent(Intent.ACTION_SEND);

        sendIntent.setType("application/octet-stream");
        sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(databaseFile));

        sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {});
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Pocket Money Manager Backup File");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "\n\nThis is a backup done on "+mFormatter.format(c.getTime())+"\n\nKeep this in a safe place!");
        startActivity(Intent.createChooser(sendIntent, "Send e-mail"));

        // startActivity(Intent.createChooser(sendIntent, getString(R.string.settings_alert_export)));

    }

    @Background
    protected void doClearDatabase() {
        onDataBaseCleared(Utils.emptyDatabase(
                ((MainActivity) getActivity()).getAccountsRepository().getConnectionSource()));
    }

    @UiThread
    protected void onDataBaseCleared(final boolean success) {
        if (null == getActivity()) {
            return;
        }

        AlertDialog.build(getActivity(), new AlertDialog.Builder() {

            @Override
            public AlertDialog buildAlertDialog(AlertDialog dialog) {
                return dialog
                        .setText(getString(success
                                ? R.string.settings_alert_cleared
                                : R.string.settings_alert_not_cleared))
                        .setPositiveButton(null, null);
            }
        }).show(getFragmentManager());
    }

    @Click(R.id.settings_rate)
    protected void rateClicked() {
        String appPackageName = getActivity().getPackageName();
        try {
            startActivityForResult(new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + appPackageName)), REQUEST_RATE_APP);
        } catch (android.content.ActivityNotFoundException e) {
            startActivityForResult(new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)), REQUEST_RATE_APP);
        }
    }

    @Click(R.id.settings_share)
    protected void shareClicked() {
        sendEmail(
                null,
                getString(R.string.share_subject),
                getString(R.string.share_message),
                getString(R.string.share_via));
    }

    @Click(R.id.settings_feedback)
    protected void feedbackClicked() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        String feedback_txt = getString(R.string.feedback_message)+"<br/><br/> SDK:"+sdkVersion + " ("+release+")"+
                "<br/> App Version: "+ BuildConfig.VERSION_CODE + " ("+BuildConfig.VERSION_NAME+")"+
                "<br/> Device: "+ getDeviceName();

        sendEmail(
                getResources().getStringArray(R.array.feedback_emails),
                getString(R.string.feedback_subject),
                feedback_txt,
                getString(R.string.feedback_via));
    }
    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    protected boolean sendEmail(String[] recipients, String subject, String message, String via) {
        try {
            Intent emailIntent = new Intent(
                    android.content.Intent.ACTION_SEND_MULTIPLE);
            emailIntent.setType("plain/text");

            if (null != recipients) {
                emailIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
            }

            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
            emailIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml( message));

            emailIntent.putExtra("exit_on_sent", true);

            startActivityForResult(Intent.createChooser(emailIntent, via), REQUEST_SEND_EMAIL);
            return true;
        } catch (Throwable t) {
            return false;
        }
    }

    @Click(R.id.settings_buy)
    protected void buyClicked() {
        ((MainActivity) getActivity()).purchasePaidVersion(new BackgroundObserver() {

            @Override
            public void onBackgroundWillStart() {
            }

            @Override
            public void onBackgroundEnded() {
                if (null != getActivity()) {
                    checkForPaidVersion();
                }
            }
        });
    }

    protected void checkForPaidVersion() {
        buy.setVisibility(((MainActivity) getActivity()).isFreeVersion() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        requestNoAds();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkForPaidVersion();
    }

    @Override
    public Title getTitleHolder() {
        return new Title(getString(R.string.settings_fragment_title));
    }
}
