package com.sqnp.pmm.content;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.util.DebugUtils;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.sqnp.pmm.R;
import com.sqnp.pmm.SharedPrefs_;
import com.sqnp.pmm.content.adapter.MenuAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.feature.BackgroundObserver;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.entity.Recurring;
import com.sqnp.pmm.entity.Reminder;
import com.sqnp.pmm.service.PaymentService;
import com.sqnp.pmm.service.db.DatabaseHelper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseFragmentActivity implements ServiceConnection {
    public static final String TAG="recurring";
    public static interface FeatureRequestFullContentFrame {}
    public static interface FeatureRequestNoAds {}

    public static final String DEFAULT_FRAGMENT = "accounts";

    public static final String EXTRA_REMINDER_ID = "reminder_id";

    @Pref
    protected SharedPrefs_ sharedPrefs;

    @SystemService
    protected AlarmManager alarmManager;

    @ViewById(R.id.main_drawer)
    protected DrawerLayout drawer;

    @ViewById(R.id.main_open_menu)
    protected ImageView openMenu;

    @ViewById(R.id.main_title_container)
    protected ViewGroup titleContainer;

    @ViewById(R.id.main_ad_fragment_container)
    protected View adFragmentContainer;

    @ViewById(R.id.main_content_frame)
    protected FrameLayout contentFrame;

    @ViewById(R.id.main_content)
    protected View content;

    @ViewById(R.id.main_drawer_left)
    protected View drawerLeft;

    @ViewById(R.id.main_menu_list)
    protected ListView menuList;

    @ViewById(R.id.main_section_title)
    protected TextView sectionTitle;

    @ViewById(R.id.main_section_menu)
    protected ViewGroup sectionMenu;

    @Bean
    protected MenuAdapter menu;

    @Bean
    protected PaymentService paymentService;

    @OrmLiteDao(helper = DatabaseHelper.class, model = Account.class)
    protected RuntimeExceptionDao<Account, Integer> accountsRepository;

    @OrmLiteDao(helper = DatabaseHelper.class, model = Recurring.class)
    protected RuntimeExceptionDao<Recurring, Integer> recurringRepository;

    @OrmLiteDao(helper = DatabaseHelper.class, model = History.class)
    protected RuntimeExceptionDao<History, Integer> historyRepository;

    @OrmLiteDao(helper = DatabaseHelper.class, model = Reminder.class)
    protected RuntimeExceptionDao<Reminder, Integer> remindersRepository;

    private volatile DatabaseHelper databaseHelper = null;


    protected IInAppBillingService googleBillingService;

    public RuntimeExceptionDao<Account, Integer> getAccountsRepository() {
        return accountsRepository;
    }

    public RuntimeExceptionDao<Recurring, Integer> getRecurringRepository() {
        return recurringRepository;
    }

    public RuntimeExceptionDao<History, Integer> getHistoryRepository() {
        return historyRepository;
    }

    public RuntimeExceptionDao<Reminder, Integer> getRemindersRepository() {
        return remindersRepository;
    }

    public SharedPrefs_ getSharedPrefs() {
        if (null == sharedPrefs.currency().get()) {
            sharedPrefs.currency().put(Currency.getInstance(Locale.getDefault()).toString());
        }

        return sharedPrefs;
    }

    public AlarmManager getAlarmManager() {
        return alarmManager;
    }


    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Uri data = getIntent().getData();
        if (data != null) {
            getIntent().setData(null);
            try {

                importData(data);

            } catch (Exception e) {
                // warn user about bad data here
                finish();
                return;
            }

            // launch home Activity (with FLAG_ACTIVITY_CLEAR_TOP) here…
        }
    }
    private void importData(Uri data) {
        if (data.equals(null)) {
            System.out.println("Data is null");
            Log.d("IMPORT", "error import file");
        } else {
            try{

                File dataFile = Environment.getDataDirectory();
                String currentDBPath = "/data/"+ "com.sqnp.pmm" +"/databases/"+"pocket_mm_db";
                File dbfile = new File(dataFile, currentDBPath);

                boolean checkdb = false;
                checkdb = dbfile.exists();

                Log.i("IMPORT", "DB Exist : " + checkdb);

                if(checkdb){
                    OpenHelperManager.releaseHelper();
                    databaseHelper = null;

                }

                copy(getContentResolver().openInputStream(data), dbfile);


                Log.d("IMPORT", " import file - done " + currentDBPath);

                databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);

                prepareViews();

            }catch (Exception e) {
                e.printStackTrace();

            }
        }
    }
    public void copy(InputStream in, File dst) throws IOException {
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }


    public DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @AfterViews
    @TargetApi(10)
    protected void prepareViews() {

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.banner_interstitial_unit_id));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                // beginPlayingGame();

                // ((MainActivity) getActivity()).pushFragment("account");

            }
        });

        requestNewInterstitial();

        drawer.setScrimColor(getResources().getColor(R.color.transparent));
        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View view, float v) {
                if (android.os.Build.VERSION.SDK_INT >= 11) {
                    int transitionSign = view.equals(drawerLeft) ? 1 : -1;
                    contentFrame.setTranslationX(transitionSign * view.getWidth() * v);
                }
            }

            @Override
            public void onDrawerOpened(View view) {
            }

            @Override
            public void onDrawerClosed(View view) {
            }

            @Override
            public void onDrawerStateChanged(int i) {
            }
        });

        menuList.setAdapter(menu);

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, this, Context.BIND_AUTO_CREATE);
        //showFragment(DEFAULT_FRAGMENT);  Causes accounts fragment to load two times, enters in conflict with drawer item selected.
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (! paymentService.handleActivityResult(this, requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void checkForRecurring(BackgroundObserver observer) {;
        if (null != observer) {
            observer.onBackgroundWillStart();
        }
        doCheckForRecurring(observer);
    }

    @Background
    protected void doCheckForRecurring(BackgroundObserver observer) {

        QueryBuilder<Recurring, Integer> queryBuilder = getRecurringRepository().queryBuilder();
        List<Recurring> recurringList;
        try {
            queryBuilder.where()
                    .le(Recurring.COLUMN_NEXT_DATE, new Date());
            recurringList = queryBuilder.query();
            if (null == recurringList) {
                throw new Exception();
            }
        } catch (Exception e) {
            onRecurringChecked(observer);
            return;
        }
        for (Recurring recurring: recurringList) {
            if (null == recurring.getAccountId()) {
                continue;
            }
            Account account = getAccountsRepository().queryForId(recurring.getAccountId());
            if (null == account) {
                continue;
            }

            do {
                History history = new History();
                history.setType(History.TYPE_RECURRING);
                history.setValue(recurring.getValue());
                history.setBalance(account.getMoney());
                history.setCurrency(account.getCurrency());
                history.setAccountId(account.getId());
                account.setMoney(account.getMoney() + recurring.getValue());
                recurring.setLastDate(new Date());
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(recurring.getNextDate());

                history.setCreatedAt(calendar.getTime());
                getHistoryRepository().create(history);

                switch (recurring.getRecurringFrequency()) {

                    case Recurring.RECURRING_FREQUENCY_DAY:
                        calendar.add(Calendar.DAY_OF_YEAR, recurring.getFrequency());
                        break;

                    case Recurring.RECURRING_FREQUENCY_WEEK:
                        calendar.add(Calendar.WEEK_OF_YEAR, recurring.getFrequency());
                        break;

                    case Recurring.RECURRING_FREQUENCY_MONTH:
                        calendar.add(Calendar.MONTH, recurring.getFrequency());
                        break;

                    case Recurring.RECURRING_FREQUENCY_YEAR:
                        calendar.add(Calendar.YEAR, recurring.getFrequency());
                }
                recurring.setNextDate(calendar.getTime());

            } while (1 > recurring.getNextDate().compareTo(new Date()));
            getAccountsRepository().update(account);
            getRecurringRepository().update(recurring);

        }
        onRecurringChecked(observer);
    }

    @UiThread
    protected void onRecurringChecked(BackgroundObserver observer) {
        if (null != observer) {
            observer.onBackgroundEnded();
        }
    }

    @Click(R.id.main_open_menu)
    protected void openMenuClicked() {
        drawer.openDrawer(Gravity.LEFT);
    }

    @ItemClick(R.id.main_menu_list)
    protected void menuListItemClicked(String item) {
        setAnimateFragment(false);
        showFragment(item, true);
        drawer.closeDrawer(Gravity.LEFT);
    }

    public void registerFragmentWithFeatures(Fragment fragment) {
        if (fragment instanceof FeatureRequestFullContentFrame) {
            titleContainer.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            adFragmentContainer.setVisibility(View.GONE);
        } else if (! (fragment instanceof FeatureRequestNoAds)) {
            titleContainer.setVisibility(View.VISIBLE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            if (isFreeVersion()) {
                adFragmentContainer.setVisibility(View.VISIBLE);
            } else {
                adFragmentContainer.setVisibility(View.GONE);
            }
        } else {
            titleContainer.setVisibility(View.VISIBLE);
            adFragmentContainer.setVisibility(View.GONE);
        }

        if (fragment instanceof SectionTitleProvider) {
            sectionMenu.removeAllViews();

            SectionTitleProvider.Title title = ((SectionTitleProvider) fragment).getTitleHolder();
            if (null != title) {
                if (title.isHidesMenu()) {
                    openMenu.setVisibility(View.GONE);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                } else {
                    openMenu.setVisibility(View.VISIBLE);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }

                title.bind(sectionTitle);

                if (title.hasMenuItems()) {
                    for (Pair<Integer, SectionTitleProvider.OnMenuItemAttachedCallback> item:
                            title.getMenuItems()) {
                        ImageView view = new ImageView(this);

                        if (null != view.getLayoutParams()) {
                            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
                            params.setMargins(0, 0, 10, 0);
                            view.setLayoutParams(params);
                        }

                        view.setImageResource(item.first);

                        sectionMenu.addView(view);
                        if (null != item.second) {
                            item.second.onMenuItemAttached(view);
                        }
                    }

                    sectionMenu.setVisibility(View.VISIBLE);
                }
            }
        } else {
            sectionTitle.setText(getString(R.string.app_name));
            sectionMenu.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        try {
            titleContainer.setVisibility(View.VISIBLE);
            adFragmentContainer.setVisibility(View.VISIBLE);
        } catch (Exception e) {}

        super.onAttachFragment(fragment);
    }

    public void hideAdFragmentContainer() {
        adFragmentContainer.setVisibility(View.GONE);
    }

    @Override
    protected int getFragmentContainer() {
        return R.id.main_fragment_container;
    }

    @Override
    protected String getDefaultFragmentName() {
        return DEFAULT_FRAGMENT;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        googleBillingService = IInAppBillingService.Stub.asInterface(service);
        paymentService.setGoogleBillingService(googleBillingService);

        menuListItemClicked(menu.getItem(0));
        content.setVisibility(View.VISIBLE);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        googleBillingService = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (null != googleBillingService) {
            unbindService(this);
        }
    }

    public boolean isFreeVersion() {
        return sharedPrefs.version().get() == getResources().getInteger(R.integer.app_version_free);
    }

    public PaymentService getPaymentService() {
        return paymentService;
    }

    public void purchasePaidVersion(final BackgroundObserver onFinish) {
        BackgroundObserver second = new BackgroundObserver() {

            protected Dialog dialog;

            @Override
            public void onBackgroundWillStart() {
                dialog = new ProgressDialog(MainActivity.this);
                dialog.setCancelable(false);
                dialog.show();

                if (null != onFinish) {
                    onFinish.onBackgroundWillStart();
                }
            }

            @Override
            public void onBackgroundEnded() {
                if (null != onFinish) {
                    onFinish.onBackgroundEnded();
                }

                if (null != dialog) {
                    dialog.dismiss();
                }
            }
        };

        getPaymentService().purchasePaidVersion(new BackgroundObserver() {

            protected Dialog dialog;

            @Override
            public void onBackgroundWillStart() {
                dialog = new ProgressDialog(MainActivity.this);
                dialog.setCancelable(false);
                dialog.show();
            }

            @Override
            public void onBackgroundEnded() {
                if (null != dialog) {
                    dialog.dismiss();
                }
            }
        }, second);
    }

    public boolean showFreeVersionRestrictionDialog() {

        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
            return true;
        } else {
            AlertDialog.build(this, new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.free_version_alert_restriction))
                            .setPositiveButton(getString(R.string.free_version_alert_restriction_positive), new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    purchasePaidVersion(null);
                                }
                            })
                            .setNegativeButton(null, null);
                }
            }).show(getSupportFragmentManager());
            return false;
        }
    }
//    public void hideSoftInputMethod() {
//        inputMethodService.hideSoftInputFromWindow(
//                getWindow().getDecorView().getRootView().getWindowToken(),
//                0);
//    }
    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

}
