package com.sqnp.pmm.content.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.feature.BindableViewHolder;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.simple_spinner_item)
public class SimpleSpinnerItemView extends LinearLayout implements
        BindableViewHolder<String> {

    @ViewById(R.id.simple_spinner_drop_down_item_title)
    protected TextView title;

    public SimpleSpinnerItemView(Context context) {
        super(context);
    }

    @Override
    public void bind(String entity) {
        title.setText(entity);
    }
}
