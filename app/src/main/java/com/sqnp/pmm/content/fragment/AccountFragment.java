package com.sqnp.pmm.content.fragment;

import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.adapter.SimpleSpinnerAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.dialog.DateDialog;
import com.sqnp.pmm.content.feature.EntityExtractor;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.Recurring;
import com.sqnp.pmm.support.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;

@EFragment(R.layout.fragment_account)
public class AccountFragment extends BaseFragment implements SectionTitleProvider {

    @FragmentArg
    protected Account account;

    protected Recurring accountRecurring;

    @ViewById(R.id.account_name)
    protected EditText name;

    @ViewById(R.id.account_currency)
    protected Spinner currency;

    @ViewById(R.id.account_amount)
    protected EditText amount;

    @ViewById(R.id.account_frequency)
    protected EditText frequency;

    @ViewById(R.id.account_recurring_frequency)
    protected Spinner recurringFrequency;

    @ViewById(R.id.account_start_date)
    protected EditText startDate;

    @Bean
    protected SimpleSpinnerAdapter<Currency> currencies;

    @Bean
    protected SimpleSpinnerAdapter<Pair<Integer, String>> recurringFrequencies;

    @AfterViews
    protected void configureViews() {
        currencies.setEntityExtractor(new EntityExtractor<Currency, String>() {

            @Override
            public String extractEntity(Currency entity) {
                return entity.toString();
            }
        });
        currencies.populate(Utils.getAvailableCurrencies(
                ((MainActivity) getActivity()).getSharedPrefs().currency().get()));
        currency.setAdapter(currencies);

        recurringFrequencies.setEntityExtractor(new EntityExtractor<Pair<Integer, String>, String>() {

            @Override
            public String extractEntity(Pair<Integer, String> entity) {
                return entity.second;
            }
        });
        List<Pair<Integer, String>> frequencies = new ArrayList<>();
        int i = 0;
        for (String frequency: getResources().getStringArray(R.array.spinner_recurring_frequency)) {
            frequencies.add(new Pair<>(i++, frequency));
        }
        recurringFrequencies.populate(frequencies);
        recurringFrequency.setAdapter(recurringFrequencies);

        populateFromAccount();
    }

    protected void populateFromAccount() {
        if (null != account) {
            currency.setFocusable(false);

            name.setText(account.getName());
            currency.setSelection(currencies.getPosition(Currency.getInstance(account.getCurrency())));

            if (null != account.getRecurringId()) {
                amount.setFocusable(false);
                frequency.setFocusable(false);
                recurringFrequency.setFocusable(false);
                startDate.setFocusable(false);

                doLoadAccountRecurring();
            }
        }
    }

    @Background
    protected void doLoadAccountRecurring() {
        try {
            accountRecurring = ((MainActivity) getActivity()).getRecurringRepository().queryForEq(
                    Recurring.COLUMN_ACCOUNT_ID, account.getId())
                    .get(0);
            populateFromAccountRecurring();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @UiThread
    protected void populateFromAccountRecurring() {
        if (null == accountRecurring) {
            return;
        }

        if (null != accountRecurring.getFrequency()) {
            frequency.setText(accountRecurring.getFrequency().toString());
        }
        if (null != accountRecurring.getRecurringFrequency()) {
            recurringFrequency.setSelection(accountRecurring.getRecurringFrequency());
        }
        if (null != accountRecurring.getNextDate()) {
            startDate.setTag(accountRecurring.getNextDate());
            startDate.setText(DateDialog.DATE_FORMAT.format(accountRecurring.getNextDate()));
        }

        amount.setText(String.valueOf(null != accountRecurring.getValue()
                ? accountRecurring.getValue()
                : 0));
    }

    @Click(R.id.account_start_date)
    protected void startDateClicked(View view) {
        DateDialog dialog = new DateDialog(getActivity());
        dialog.setEditText((EditText) view);

        dialog.show();
    }

    @Click(R.id.account_save)
    protected void saveClicked() {
        AlertDialog dialog = null;
        if (TextUtils.isEmpty(name.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.account_alert_no_name))
                            .setPositiveButton(null, null);
                }
            });
        } else if (0 != ((Pair<Integer, String>) recurringFrequency.getSelectedItem()).first) {
            if (TextUtils.isEmpty(amount.getText())) {
                dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                    @Override
                    public AlertDialog buildAlertDialog(AlertDialog dialog) {
                        return dialog
                                .setText(getString(R.string.account_alert_no_amount))
                                .setPositiveButton(null, null);
                    }
                });
            } else if (TextUtils.isEmpty(startDate.getText())) {
                dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                    @Override
                    public AlertDialog buildAlertDialog(AlertDialog dialog) {
                        return dialog
                                .setText(getString(R.string.account_alert_no_start_date))
                                .setPositiveButton(null, null);
                    }
                });
            } else {
                try {
                    if (0 >= Integer.parseInt(String.valueOf(this.frequency.getText()))) {
                        throw new Exception();
                    }
                } catch (Exception e) {
                    dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                        @Override
                        public AlertDialog buildAlertDialog(AlertDialog dialog) {
                            return dialog
                                    .setText(getString(R.string.account_alert_no_frequency))
                                    .setPositiveButton(null, null);
                        }
                    });
                }
            }
        }

        if (null != dialog) {
            dialog.show(getFragmentManager());
            return;
        }

        doSave();
    }

    @Background
    protected void doSave() {
        // Edit ?
        if (null != account) {
            populateAccount(account);
            if (null == accountRecurring) {
                accountRecurring = new Recurring();
                if (populateAccountRecurring(accountRecurring)) {
                    accountRecurring.setAccountId(account.getId());
                    ((MainActivity) getActivity()).getRecurringRepository().create(accountRecurring);
                    account.setRecurringId(accountRecurring.getId());
                }
            }

            ((MainActivity) getActivity()).getAccountsRepository().update(account);

            onSaved();
            return;
        }

        // Save?
        Account account = new Account();
        account.setCreatedAt(new Date());

        populateAccount(account);
        ((MainActivity) getActivity()).getAccountsRepository().create(account);

        // Recurring
        Recurring recurring = new Recurring();
        if (populateAccountRecurring(recurring)) {
            recurring.setAccountId(account.getId());

            ((MainActivity) getActivity()).getRecurringRepository().create(recurring);
            account.setRecurringId(recurring.getId());
            ((MainActivity) getActivity()).getAccountsRepository().update(account);
        }

        onSaved();
    }

    protected void populateAccount(Account account) {
        account.setName(name.getText().toString());
        account.setCurrency(currency.getSelectedItem().toString());

        if (null == account.getId()) {
            account.setMoney(0.0);
        }
    }

    protected boolean populateAccountRecurring(Recurring recurring) {
        int frequency = ((Pair<Integer, String>) recurringFrequency.getSelectedItem()).first;
        if (0 != frequency) {
            recurring.setFrequency(Integer.parseInt(this.frequency.getText().toString()));
            recurring.setRecurringFrequency(frequency);
            recurring.setLastDate(null);
            recurring.setNextDate((Date) startDate.getTag());
                recurring.setValue(Double.parseDouble(amount.getText().toString()));

            return true;
        }

        return false;
    }

    @UiThread
    protected void onSaved() {
        if (null != getActivity()) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public Title getTitleHolder() {
        Title title = new Title(getString(R.string.account_fragment_title));
        title.setHidesMenu(null != getActivity()
                && 0 != ((MainActivity) getActivity()).getAccountsRepository().countOf());

        return title;
    }
}
