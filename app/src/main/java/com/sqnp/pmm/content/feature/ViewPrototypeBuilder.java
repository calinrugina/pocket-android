package com.sqnp.pmm.content.feature;

public interface ViewPrototypeBuilder<T> {

    public T buildViewPrototype(T view);
}
