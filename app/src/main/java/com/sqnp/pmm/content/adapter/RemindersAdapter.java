package com.sqnp.pmm.content.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.sqnp.pmm.content.feature.ViewPrototypeBuilder;
import com.sqnp.pmm.content.view.AccountAwareHeaderItemView;
import com.sqnp.pmm.content.view.AccountAwareHeaderItemView_;
import com.sqnp.pmm.content.view.ReminderItemView;
import com.sqnp.pmm.content.view.ReminderItemView_;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.Reminder;

import org.androidannotations.annotations.EBean;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

@EBean
public class RemindersAdapter extends AccountAwareProgressiveLoaderAdapter<Reminder> implements
        StickyListHeadersAdapter {

    protected Account account;
    protected RuntimeExceptionDao<Reminder, Integer> remindersRepository;
    protected ViewPrototypeBuilder<ReminderItemView> viewPrototypeBuilder;

    public void setAccount(Account account) {
        this.account = account;
    }

    public void setRemindersRepository(RuntimeExceptionDao<Reminder, Integer> remindersRepository) {
        this.remindersRepository = remindersRepository;
    }

    public void setViewPrototypeBuilder(ViewPrototypeBuilder<ReminderItemView> viewPrototypeBuilder) {
        this.viewPrototypeBuilder = viewPrototypeBuilder;
    }

    @Override
    public Response<Reminder> loadData(Request request) {
        try {
            QueryBuilder<Reminder, Integer> remindersQueryBuilder = remindersRepository.queryBuilder();

            if (null != account) {
                remindersQueryBuilder.where()
                        .eq(Reminder.COLUMN_ACCOUNT_ID, account.getId());
            }

            remindersQueryBuilder.orderBy(Reminder.COLUMN_NEXT_DATE, true);

            Response<Reminder> response = new Response<>();
            response.setTotalCount(remindersRepository.countOf(remindersQueryBuilder.setCountOf(true).prepare()));

            remindersQueryBuilder.limit(Long.valueOf(String.valueOf(request.getLimit())));
            remindersQueryBuilder.offset(Long.valueOf(String.valueOf(request.getOffset())));

            List<Reminder> reminders = remindersRepository.query(remindersQueryBuilder.setCountOf(false).prepare());

            response.setData(reminders);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        AccountAwareHeaderItemView itemView;
        if (null == view) {
            itemView = AccountAwareHeaderItemView_.build(viewGroup.getContext());
        } else {
            try {
                itemView = (AccountAwareHeaderItemView) view;
            } catch (Exception e) {
                itemView = AccountAwareHeaderItemView_.build(viewGroup.getContext());
            }
        }

        itemView.bind(getItem(i));
        return itemView;
    }

    @Override
    public long getHeaderId(int i) {
        if (null != getItem(i) && null != getItem(i).getAccountId()) {
            return getItem(i).getAccountId();
        }

        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ReminderItemView itemView;
        if (null == convertView) {
            itemView = null != viewPrototypeBuilder
                    ? viewPrototypeBuilder.buildViewPrototype(ReminderItemView_.build(parent.getContext()))
                    : ReminderItemView_.build(parent.getContext());
        } else {
            itemView = (ReminderItemView) convertView;
        }

        itemView.bind(getItem(position));
        return itemView;
    }
}
