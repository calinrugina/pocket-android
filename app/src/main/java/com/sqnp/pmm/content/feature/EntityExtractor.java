package com.sqnp.pmm.content.feature;

public interface EntityExtractor<T, A> {

    public A extractEntity(T entity);
}
