package com.sqnp.pmm.content.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.sqnp.pmm.R;
import com.sqnp.pmm.content.BaseFragmentActivity;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.ProgressDialog;
import com.sqnp.pmm.content.adapter.AccountsAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.feature.BackgroundObserver;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.content.feature.ViewPrototypeBuilder;
import com.sqnp.pmm.content.view.AccountItemView;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.entity.Recurring;
import com.sqnp.pmm.entity.Reminder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_accounts)
public class AccountsFragment extends BaseFragment implements
        SectionTitleProvider,
        AccountItemView.EditClickedListener,
        AccountItemView.RemoveClickedListener {

    @ViewById(R.id.accounts_accounts_list)
    protected ListView accountsList;

    @Bean
    protected AccountsAdapter accounts;

    @AfterViews
    public void configureViews() {
        Log.i(getClass().getName(),"AccountsFragment has been created");
        accounts.setAccountsRepository(((MainActivity) getActivity()).getAccountsRepository());
        accounts.setViewPrototypeBuilder(new ViewPrototypeBuilder<AccountItemView>() {

            @Override
            public AccountItemView buildViewPrototype(AccountItemView view) {
                view.setEditClickedListener(AccountsFragment.this);
                view.setRemoveClickedListener(AccountsFragment.this);
                view.setRecurringRepository(((MainActivity) getActivity()).getRecurringRepository());

                return view;
            }
        });
        accountsList.setAdapter(accounts);
    }

    @Override
    public void onEditAccountClicked(final Account account) {
        ((MainActivity) getActivity()).pushFragment("account", new BaseFragmentActivity.FragmentBuilder() {

            @Override
            public void buildFragment(Bundle fragmentBundle) {
                fragmentBundle.putSerializable("account", account);
            }
        });
    }

    @Override
    public void onRemoveAccountClicked(final Account account) {
        AlertDialog.build(getActivity(), new AlertDialog.Builder() {

            @Override
            public AlertDialog buildAlertDialog(AlertDialog dialog) {
                return dialog
                        .setText(getString(R.string.accounts_alert_remove, account.getName()))
                        .setPositiveButton(getString(R.string.accounts_alert_remove_positive), new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (null == getActivity()) {
                                    return;
                                }

                                Dialog progress = new ProgressDialog(getActivity());
                                progress.setCancelable(false);
                                progress.show();

                                doRemoveAccount(account, progress);
                            }
                        })
                        .setNegativeButton(null, null);
            }
        }).show(getFragmentManager());
    }

    @Background
    protected void doRemoveAccount(Account account, Dialog progress) {
        try {
            DeleteBuilder<Recurring, Integer> deleteBuilder = ((MainActivity) getActivity()).getRecurringRepository().deleteBuilder();
            deleteBuilder
                    .where()
                    .eq(Recurring.COLUMN_ACCOUNT_ID, account.getId());
            deleteBuilder.delete();

            DeleteBuilder<History, Integer> deleteHistory = ((MainActivity) getActivity()).getHistoryRepository().deleteBuilder();
            deleteHistory
                    .where()
                    .eq(History.COLUMN_ACCOUNT_ID, account.getId());
            deleteHistory.delete();

            DeleteBuilder<Reminder, Integer> deleteReminder = ((MainActivity) getActivity()).getRemindersRepository().deleteBuilder();
            deleteReminder
                    .where()
                    .eq(Reminder.COLUMN_ACCOUNT_ID, account.getId());
            deleteReminder.delete();

            ((MainActivity) getActivity()).getAccountsRepository().delete(account);
        } catch (Exception e) {
            e.printStackTrace();
        }

        onAccountRemoved(progress);
    }

    @UiThread
    protected void onAccountRemoved(Dialog progress) {
        if (null == getActivity()) {
            return;
        }

        progress.dismiss();

        if (0 == ((MainActivity) getActivity()).getAccountsRepository().countOf()) {
            ((BaseFragmentActivity) getActivity()).pushFragment("account");
            return;
        }

        accounts.load();
    }

    @ItemClick(R.id.accounts_accounts_list)
    public void accountsListItemClicked(final Account account) {
        ((BaseFragmentActivity) getActivity()).pushFragment("transactions", new BaseFragmentActivity.FragmentBuilder() {

            @Override
            public void buildFragment(Bundle fragmentBundle) {
                fragmentBundle.putSerializable("account", account);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (0 == ((MainActivity) getActivity()).getAccountsRepository().countOf()) {
            ((MainActivity) getActivity()).pushFragment("account");
        }

        ((MainActivity) getActivity()).checkForRecurring(new BackgroundObserver() {

            protected Dialog dialog;

            @Override
            public void onBackgroundWillStart() {
                dialog = new ProgressDialog(getActivity());
                dialog.setCancelable(false);
                dialog.show();
            }

            @Override
            public void onBackgroundEnded() {
                accounts.load();

                if (null != dialog) {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public Title getTitleHolder() {
        return new Title(getString(R.string.accounts_fragment_title),
                new Pair<Integer, OnMenuItemAttachedCallback>(R.drawable.ic_action_add, new OnMenuItemAttachedCallback() {

                    @Override
                    public void onMenuItemAttached(ImageView view) {
                        view.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (((MainActivity) getActivity()).isFreeVersion()
                                        && ((MainActivity) getActivity()).getAccountsRepository().countOf()
                                        >= getResources().getInteger(R.integer.free_version_max_accounts)) {

                                    if ( ((MainActivity) getActivity()).showFreeVersionRestrictionDialog() )
                                        ((BaseFragmentActivity) getActivity()).pushFragment("account");
                                    return;
                                }

                                ((BaseFragmentActivity) getActivity()).pushFragment("account");
                            }
                        });
                    }
                }));
    }
}
