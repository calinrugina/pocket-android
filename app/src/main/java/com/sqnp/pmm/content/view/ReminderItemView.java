package com.sqnp.pmm.content.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.dialog.DateDialog;
import com.sqnp.pmm.content.feature.BindableViewHolder;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.entity.Reminder;
import com.sqnp.pmm.support.Utils;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.reminder_item)
public class ReminderItemView extends LinearLayout implements BindableViewHolder<Reminder> {

    public static interface TransactionClickedListener {

        public void onTransactionReminderClicked(Reminder reminder);
    }

    public static interface RemoveClickedListener {

        public void onRemoveReminderClicked(Reminder reminder);
    }

    @ViewById(R.id.reminder_item_title)
    protected TextView title;

    @ViewById(R.id.reminder_item_date)
    protected TextView date;

    @ViewById(R.id.reminder_item_value)
    protected TextView value;

    protected TransactionClickedListener transactionClickedListener;
    protected RemoveClickedListener removeClickedListener;

    public ReminderItemView(Context context) {
        super(context);
    }

    public void setTransactionClickedListener(TransactionClickedListener transactionClickedListener) {
        this.transactionClickedListener = transactionClickedListener;
    }

    public void setRemoveClickedListener(RemoveClickedListener removeClickedListener) {
        this.removeClickedListener = removeClickedListener;
    }

    @Override
    public void bind(Reminder entity) {
        setTag(entity);

        title.setText(entity.getTitle());
        date.setText(DateDialog.DATE_TIME_FORMAT.format(entity.getNextDate()));
        value.setText(Utils.formatBalance(
                entity.getValue() * (entity.isOfType(History.TYPE_DEBIT) ? -1 : 1),
                entity.getCurrency()));
    }

    @Click(R.id.reminder_item_transaction)
    protected void transactionClicked() {
        if (null != transactionClickedListener) {
            transactionClickedListener.onTransactionReminderClicked((Reminder) getTag());
        }
    }

    @Click(R.id.reminder_item_remove)
    protected void removeClicked() {
        if (null != removeClickedListener) {
            removeClickedListener.onRemoveReminderClicked((Reminder) getTag());
        }
    }
}
