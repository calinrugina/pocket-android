package com.sqnp.pmm.content.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sqnp.pmm.content.feature.EntityExtractor;
import com.sqnp.pmm.content.view.SimpleSpinnerDropDownItemView;
import com.sqnp.pmm.content.view.SimpleSpinnerDropDownItemView_;
import com.sqnp.pmm.content.view.SimpleSpinnerItemView;
import com.sqnp.pmm.content.view.SimpleSpinnerItemView_;

import org.androidannotations.annotations.EBean;

import java.util.List;

@EBean
public class SimpleSpinnerAdapter<T> extends BaseAdapter {

    protected List<T> data;
    protected EntityExtractor<T, String> entityExtractor;

    public void setEntityExtractor(EntityExtractor<T, String> entityExtractor) {
        this.entityExtractor = entityExtractor;
    }

    public void populate(List<T> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return null != data ? data.size() : 0;
    }

    @Override
    public T getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleSpinnerItemView itemView;
        if (null == convertView) {
            itemView =  SimpleSpinnerItemView_.build(parent.getContext());
        } else {
            itemView = (SimpleSpinnerItemView) convertView;
        }

        itemView.bind(entityExtractor.extractEntity(getItem(position)));
        return itemView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        SimpleSpinnerDropDownItemView itemView;
        if (null == convertView) {
            itemView =  SimpleSpinnerDropDownItemView_.build(parent.getContext());
        } else {
            itemView = (SimpleSpinnerDropDownItemView) convertView;
        }

        itemView.bind(entityExtractor.extractEntity(getItem(position)));
        return itemView;
    }

    public int getPosition(T item) {
        if (null == data) {
            return -1;
        }

        for (int i=0; i<data.size(); i++) {
            if (data.get(i).equals(item)) {
                return i;
            }
        }

        return -1;
    }
}
