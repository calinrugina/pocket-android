package com.sqnp.pmm.content;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.sqnp.pmm.R;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

abstract public class BaseFragmentActivity extends FragmentActivity {

    private boolean backPressedFirstTime = false;

    public static interface FragmentBuilder {

        public void buildFragment(Bundle fragmentBundle);
    }

    protected boolean animateFragment = false;
    protected boolean animateFragmentTransitions = true;

    protected Fragment shownFragment;
    protected String shownFragmentName;

    abstract protected int getFragmentContainer();

    abstract protected String getDefaultFragmentName();

    public void setAnimateFragment(boolean animateFragment) {
        this.animateFragment = animateFragment;
    }

    protected String getFragmentTagFromName(String name) {
        return String.format("%s_fragment", name);
    }

    public Fragment getFragmentByName(String name, FragmentBuilder builder) {
        Fragment fragment = getFragmentByName(name);
        if (null == fragment) {
            return null;
        }

        if (null == builder) {
            return fragment;
        }

        Bundle bundle = new Bundle();
        builder.buildFragment(bundle);

        try {
            fragment.setArguments(bundle);
        } catch (IllegalStateException e) {
            if (null != fragment.getArguments()) {
                replaceFragmentArguments(fragment, bundle);
            }
        }

        return fragment;
    }

    protected void replaceFragmentArguments(final Fragment fragment, Bundle args) {
        Map<String, Field> fragmentArgs = new HashMap<String, Field>() {{
            for (Field field: ((Object) fragment).getClass().getDeclaredFields()) {
                put(field.getName(), field);
            }
        }};

        for (String arg: args.keySet()) {
            if (! fragmentArgs.containsKey(arg)) {
                continue;
            }

            Field fragmentArg = fragmentArgs.get(arg);
            fragmentArg.setAccessible(true);
            try {
                fragmentArg.set(fragment, args.get(arg));
            } catch (Exception e) {
                continue;
            } finally {
                fragmentArg.setAccessible(false);
            }

            fragmentArg.setAccessible(false);
        }
    }

    public Fragment getFragmentByName(String name) {
        try {
            return (Fragment) getFragmentClassByName(name).newInstance();
        } catch (Exception e) {
            return null;
        }
    }

    public Class<?> getFragmentClassByName(String name) {
        try {
            Class<?> fragmentClass = Class.forName(getResources().getString(
                    getResources().getIdentifier(getFragmentTagFromName(name), "string", getPackageName())));
            if (! Fragment.class.isAssignableFrom(fragmentClass)) {
                throw new ClassNotFoundException(String.format("%s is not a Fragment",
                        fragmentClass));
            }

            return fragmentClass;
        } catch (Exception e) {
            return null;
        }
    }

    public Fragment showFragment(String name) {
        Log.i(getClass().getName(),"Show Fragment 1");
        return showFragment(name, false);
    }

    public Fragment showFragment(String name, boolean clearStack) {
        Log.i(getClass().getName(),"Show Fragment 2");
        return showFragment(name, getFragmentByName(name), clearStack);
    }

    public Fragment showFragment(String name, Fragment fragment, boolean clearStack) {
        if (clearStack) {
            try {
                clearFragmentBackStack();
            } catch (Exception e){
            }
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (animateFragment) {
            transaction.setCustomAnimations(
                    R.anim.slide_in_right,
                    R.anim.slide_out_left,
                    R.anim.slide_out_right,
                    R.anim.slide_in_left);
        }

        transaction.replace(getFragmentContainer(), fragment,
                getFragmentTagFromName(name));

        transaction.addToBackStack(null);

        shownFragment = fragment;
        shownFragmentName = name;

        transaction.commitAllowingStateLoss();
        Log.i(getClass().getName(),"Show Fragment 3");
        return fragment;
    }

    public void pushFragment(String tag, FragmentBuilder builder) {
        pushFragment(tag, builder, true);
    }

    public void pushFragment(String tag) {
        pushFragment(tag, null, true);
    }

    public void pushFragment(String tag, boolean animate) {
        pushFragment(tag, null, animate);
    }

    public void pushFragment(String tag, FragmentBuilder builder, boolean animate) {
        animateFragment = animate;
        showFragment(tag, getFragmentByName(tag, builder), false);
    }

    public void clearFragmentBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        if (0 < manager.getBackStackEntryCount()) {
            animateFragmentTransitions = false;

            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStackImmediate(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

            animateFragmentTransitions = true;
        }
    }

    public Fragment getShownFragment() {
        return shownFragment;
    }

    @Override
    public void onBackPressed() {
        if (1 < getSupportFragmentManager().getBackStackEntryCount()) {
            super.onBackPressed();
        } else {
            finish();
        }
    }
}
