package com.sqnp.pmm.content;

import android.content.Context;
import android.os.Bundle;

import com.sqnp.pmm.R;

public class ProgressDialog extends android.app.ProgressDialog {

    public ProgressDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.progress_dialog);
    }
}
