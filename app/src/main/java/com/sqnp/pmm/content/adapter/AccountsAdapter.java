package com.sqnp.pmm.content.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sqnp.pmm.content.feature.BackgroundObserver;
import com.sqnp.pmm.content.feature.ViewPrototypeBuilder;
import com.sqnp.pmm.content.view.AccountItemView;
import com.sqnp.pmm.content.view.AccountItemView_;
import com.sqnp.pmm.entity.Account;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.util.List;

@EBean
public class AccountsAdapter extends BaseAdapter {

    protected BackgroundObserver observer;
    protected List<Account> accounts;
    protected ViewPrototypeBuilder<AccountItemView> viewPrototypeBuilder;
    protected RuntimeExceptionDao<Account, Integer> accountsRepository;

    public void setAccountsRepository(RuntimeExceptionDao<Account, Integer> accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    public void setObserver(BackgroundObserver observer) {
        this.observer = observer;
    }

    public void setViewPrototypeBuilder(ViewPrototypeBuilder<AccountItemView> viewPrototypeBuilder) {
        this.viewPrototypeBuilder = viewPrototypeBuilder;
    }

    public void load() {
        if (null != observer) {
            observer.onBackgroundWillStart();
        }

        doLoad();
    }

    @Background
    protected void doLoad() {
        onLoaded(accountsRepository.queryForAll());
    }

    @UiThread
    protected void onLoaded(List<Account> accounts) {
        this.accounts = accounts;

        notifyDataSetChanged();
        if (null != observer) {
            observer.onBackgroundEnded();
        }
    }

    @Override
    public int getCount() {
        return null != accounts ? accounts.size() : 0;
    }

    @Override
    public Account getItem(int position) {
        return accounts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return accounts.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AccountItemView itemView;
        if (null == convertView) {
            itemView = null != viewPrototypeBuilder
                    ? viewPrototypeBuilder.buildViewPrototype(
                        AccountItemView_.build(parent.getContext()))
                    : AccountItemView_.build(parent.getContext()) ;
        } else {
            itemView = (AccountItemView) convertView;
        }

        itemView.bind(getItem(position));
        return itemView;
    }
}
