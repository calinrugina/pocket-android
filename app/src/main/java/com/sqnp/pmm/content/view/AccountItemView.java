package com.sqnp.pmm.content.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.sqnp.pmm.R;
import com.sqnp.pmm.content.feature.BindableViewHolder;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.Recurring;
import com.sqnp.pmm.support.Utils;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.account_item)
public class AccountItemView extends LinearLayout implements BindableViewHolder<Account> {

    public static interface EditClickedListener {
        
        public void onEditAccountClicked(Account account);
    }

    public static interface RemoveClickedListener {

        public void onRemoveAccountClicked(Account account);
    }

    @ViewById(R.id.account_item_name)
    protected TextView name;

    @ViewById(R.id.account_item_details)
    protected TextView details;

    @ViewById(R.id.account_item_balance)
    protected TextView balance;

    protected RuntimeExceptionDao<Recurring, Integer> recurringRepository;

    protected EditClickedListener editClickedListener;
    protected RemoveClickedListener removeClickedListener;

    public AccountItemView(Context context) {
        super(context);
    }

    public void setEditClickedListener(EditClickedListener editClickedListener) {
        this.editClickedListener = editClickedListener;
    }

    public void setRemoveClickedListener(RemoveClickedListener removeClickedListener) {
        this.removeClickedListener = removeClickedListener;
    }

    public void setRecurringRepository(RuntimeExceptionDao<Recurring, Integer> recurringRepository) {
        this.recurringRepository = recurringRepository;
    }

    @Override
    public void bind(Account entity) {
        setTag(entity);

        name.setText(entity.getName());
        balance.setText(Utils.formatBalance(entity.getMoney(), entity.getCurrency()));
        details.setText(null);

        if (null == entity.getRecurringId()) {
            return;
        }

        doLoadRecurring();
    }

    @Background
    protected void doLoadRecurring() {
        onRecurringLoaded(recurringRepository.queryForId(((Account) getTag()).getRecurringId()));
    }

    @UiThread
    protected void onRecurringLoaded(Recurring recurring) {
        if (null == getContext() || null == recurring
                || null == recurring.getRecurringFrequency()
                ||  0 == recurring.getRecurringFrequency()) {
            return;
        }

        details.setText(getContext().getString(
                R.string.account_item_details,
                Utils.formatBalance(recurring.getValue(), ((Account) getTag()).getCurrency()),
                recurring.getFrequency(),
                getContext().getResources().getStringArray(
                        R.array.spinner_recurring_frequency)[recurring.getRecurringFrequency()]
                        .toLowerCase()));
    }

    @Click(R.id.account_item_edit)
    protected void editClicked() {
        if (null != editClickedListener) {
            editClickedListener.onEditAccountClicked((Account) getTag());
        }
    }

    @Click(R.id.account_item_remove)
    protected void removeClicked() {
        if (null != removeClickedListener) {
            removeClickedListener.onRemoveAccountClicked((Account) getTag());
        }
    }
}
