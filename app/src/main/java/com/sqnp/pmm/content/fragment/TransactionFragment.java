package com.sqnp.pmm.content.fragment;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.widget.Spinner;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.adapter.SimpleSpinnerAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.feature.EntityExtractor;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.support.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EFragment(R.layout.fragment_transaction)
public class TransactionFragment extends BaseFragment implements SectionTitleProvider {

    @FragmentArg
    protected Account account;

    @ViewById(R.id.transaction_title)
    protected TextView title;

    @ViewById(R.id.transaction_type)
    protected Spinner type;

    @ViewById(R.id.transaction_amount)
    protected TextView amount;

    @Bean
    protected SimpleSpinnerAdapter<Pair<Integer, String>> types;

    @AfterViews
    protected void prepareViews() {
        List<Pair<Integer, String>> transactionTypes = new ArrayList<>();
        int i = 1;
        for (String transactionType : getResources().getStringArray(R.array.spinner_transaction_type)) {
            transactionTypes.add(new Pair<>(i++, transactionType));
        }
        types.setEntityExtractor(new EntityExtractor<Pair<Integer, String>, String>() {
            @Override
            public String extractEntity(Pair<Integer, String> entity) {
                return entity.second;
            }
        });
        types.populate(transactionTypes);
        type.setAdapter(types);
    }

    @Click(R.id.transaction_save)
    protected void saveClicked() {
        AlertDialog dialog = null;
        if (TextUtils.isEmpty(title.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.transaction_alert_no_title))
                            .setPositiveButton(null, null);
                }
            });
        } else if (TextUtils.isEmpty(amount.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.transaction_alert_no_amount))
                            .setPositiveButton(null, null);
                }
            });
        }
        if (null != dialog) {
            dialog.show(getFragmentManager());
            return;
        }
        doSaveTransaction();
    }

    @Background
    protected void doSaveTransaction() {
        History history = new History();
        history.setAccountId(account.getId());
        history.setCreatedAt(new Date());
        history.setCurrency(account.getCurrency());
        history.setBalance(account.getMoney());
        history.setTitle(title.getText().toString());
        history.setValue(Double.parseDouble(amount.getText().toString()));
        history.setType(((Pair<Integer, String>) type.getSelectedItem()).first);

        ((MainActivity) getActivity()).getHistoryRepository().create(history);
        account.setMoney(account.getMoney()
                + history.getValue() * (history.isOfType(History.TYPE_DEBIT) ? -1 : 1));
        ((MainActivity) getActivity()).getAccountsRepository().update(account);
        onTransactionSaved();
        Log.v(getClass().getName(),"Sum after adding: "+account.getMoney());
    }

    @UiThread
    protected void onTransactionSaved() {

        if (null != getActivity()) {
//            ((BaseFragmentActivity) getActivity()).pushFragment("transactions", new BaseFragmentActivity.FragmentBuilder() {
//
//                @Override
//                public void buildFragment(Bundle fragmentBundle) {
//                    fragmentBundle.putSerializable("account", account);
//                }
//            });
            getActivity().onBackPressed();
        }
    }

    @Override
    public Title getTitleHolder() {
        Title title;
        if (null == account) {
            title = new Title(null);
        } else {
            title = new Title(getString(R.string.transaction_fragment_title, account.getName()+
                    "\nTotal: "+
                     Utils.formatBalance(account.getMoney(), account.getCurrency())));
        }
        title.setHidesMenu(true);
        return title;
    }
}
