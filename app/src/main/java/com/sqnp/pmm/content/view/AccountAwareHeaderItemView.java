package com.sqnp.pmm.content.view;

import android.content.Context;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.feature.BindableViewHolder;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.support.Utils;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.account_aware_header_item)
public class AccountAwareHeaderItemView extends LinearLayout implements
        BindableViewHolder<Account.AccountAwareEntity> {

    @ViewById(R.id.account_aware_header_item_account)
    protected TextView account;

    public AccountAwareHeaderItemView(Context context) {
        super(context);
    }

    @Override
    public void bind(Account.AccountAwareEntity entity) {

        Log.d("Money", Utils.formatBalance(entity.getAccount().getMoney(), entity.getAccount().getCurrency()));

        account.setText(entity.getAccount().getName()
               // + "\nTotal: "+ Utils.formatBalance(entity.getAccount().getMoney(), entity.getAccount().getCurrency())
        );
    }
}
