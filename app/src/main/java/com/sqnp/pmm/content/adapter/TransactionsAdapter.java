package com.sqnp.pmm.content.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.sqnp.pmm.content.feature.ViewPrototypeBuilder;
import com.sqnp.pmm.content.view.AccountAwareHeaderItemView;
import com.sqnp.pmm.content.view.AccountAwareHeaderItemView_;
import com.sqnp.pmm.content.view.TransactionItemView;
import com.sqnp.pmm.content.view.TransactionItemView_;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;

import org.androidannotations.annotations.EBean;

import java.util.Date;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

@EBean
public class TransactionsAdapter extends AccountAwareProgressiveLoaderAdapter<History> implements
        StickyListHeadersAdapter,
        AbstractProgressiveLoaderAdapter.CustomRequestProvider {

    public static class FiltrableRequest extends Request {

        protected Date startDate;
        protected Date endDate;
        protected Account account;

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }
    }

    protected Account account;

    protected RuntimeExceptionDao<History, Integer> historyRepository;

    protected FiltrableRequest pendingRequest;

    protected ViewPrototypeBuilder<TransactionItemView> viewPrototypeBuilder;

    @Override
    public Request getPendingRequest(Request request) {
        Request mRequest = getPendingRequest();
        mRequest.setLimit(request.getLimit());
        mRequest.setOffset(request.getOffset());

        return mRequest;
    }

    public FiltrableRequest getPendingRequest() {
        if (null == pendingRequest) {
            pendingRequest = new FiltrableRequest();
        }

        return pendingRequest;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void setHistoryRepository(RuntimeExceptionDao<History, Integer> historyRepository) {
        this.historyRepository = historyRepository;
    }

    public void setViewPrototypeBuilder(ViewPrototypeBuilder<TransactionItemView> viewPrototypeBuilder) {
        this.viewPrototypeBuilder = viewPrototypeBuilder;
    }

    @Override
    public void reset() {
        super.reset();

        pendingRequest = null;
    }

    @Override
    public Response<History> loadData(Request request) {
        try {
            QueryBuilder<History, Integer> historyQueryBuilder = historyRepository.queryBuilder();

            if (null != account) {
                historyQueryBuilder.where()
                        .eq(History.COLUMN_ACCOUNT_ID, account.getId());
            } else if (null != getPendingRequest()) {
                if (null != getPendingRequest().getAccount()) {
                    historyQueryBuilder.where()
                            .eq(History.COLUMN_ACCOUNT_ID, getPendingRequest().getAccount().getId());
                }

                if (null != getPendingRequest().getStartDate()) {
                    historyQueryBuilder.where()
                            .ge(History.COLUMN_CREATED_AT, getPendingRequest().getStartDate());
                }

                if (null != getPendingRequest().getEndDate()) {
                    historyQueryBuilder.where()
                            .le(History.COLUMN_CREATED_AT, getPendingRequest().getEndDate());
                }
            }

            historyQueryBuilder.orderBy(History.COLUMN_CREATED_AT, false);

            Response<History> response = new Response<>();
            response.setTotalCount(historyRepository.countOf(historyQueryBuilder.setCountOf(true).prepare()));
            historyQueryBuilder.limit(Long.valueOf(String.valueOf(request.getLimit())));
            historyQueryBuilder.offset(Long.valueOf(String.valueOf(request.getOffset())));
            List<History> transactions = historyRepository.query(
                    historyQueryBuilder.setCountOf(null).prepare()
            );
            response.setData(transactions);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup) {
        if (isFiltersApplied()) {
            return new View(viewGroup.getContext());
        }

        AccountAwareHeaderItemView itemView;
        if (null == view) {
            itemView = AccountAwareHeaderItemView_.build(viewGroup.getContext());
        } else {
            try {
                itemView = (AccountAwareHeaderItemView) view;
            } catch (Exception e) {
                itemView = AccountAwareHeaderItemView_.build(viewGroup.getContext());
            }
        }

        itemView.bind(getItem(i));
        return itemView;
    }

    @Override
    public long getHeaderId(int i) {
        if (null != getItem(i) && null != getItem(i).getAccountId()) {
            return getItem(i).getAccountId();
        }

        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TransactionItemView itemView;
        if (null == convertView) {
            itemView = null != viewPrototypeBuilder
                    ? viewPrototypeBuilder.buildViewPrototype(
                    TransactionItemView_.build(parent.getContext()))
                    : TransactionItemView_.build(parent.getContext());
        } else {
            itemView = (TransactionItemView) convertView;
        }

        itemView.bind(getItem(position));
        return itemView;
    }

    protected boolean isFiltersApplied() {
        return null != getPendingRequest().getAccount();
    }

    @Override
    protected void onLoaded(List<History> data) {
        if (isFiltersApplied() && null != data) {
            History history = new History();
            history.setValue(0.0);
            history.setCurrency(getPendingRequest().getAccount().getCurrency());
            for (History old : data) {
                history.setValue(history.getValue()
                        + old.getValue() * (old.isOfType(History.TYPE_DEBIT) ? -1 : 1));
            }

            data.add(history);
        }

        super.onLoaded(data);
    }

    @Override
    public boolean isEnabled(int position) {
        return ! getItem(position).isOfType(History.TYPE_RECURRING);
    }
}
