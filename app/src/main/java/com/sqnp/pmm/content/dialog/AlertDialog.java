package com.sqnp.pmm.content.dialog;

import android.app.Activity;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.method.LinkMovementMethod;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sqnp.pmm.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.dialog_alert)
public class AlertDialog extends DialogFragment {

    public static interface Builder {

        public AlertDialog buildAlertDialog(AlertDialog dialog);
    }

    public interface DialogDismissListener {

        public void onDialogDismiss();
    }

    @ViewById(R.id.alert_title)
    protected TextView title;

    @ViewById(R.id.alert_text)
    protected TextView text;

    @ViewById(R.id.alert_negative_button)
    protected Button negativeButton;

    @ViewById(R.id.alert_positive_button)
    protected Button positiveButton;

    protected Pair<String, View.OnClickListener> positivePair;
    protected Pair<String, View.OnClickListener> negativePair;

    protected String hasTitle;
    protected CharSequence hasText;

    protected boolean dismissOnPositiveClick = false;

    protected DialogDismissListener dismissListener;

    public AlertDialog setNegativeButton(String text, View.OnClickListener listener) {
        negativePair = new Pair<String, View.OnClickListener>(text, listener);
        return this;
    }

    public AlertDialog setPositiveButton(String text, View.OnClickListener listener) {
        positivePair = new Pair<String, View.OnClickListener>(text, listener);
        return this;
    }

    public AlertDialog setTitle(String text) {
        hasTitle = text;
        return this;
    }

    public AlertDialog setText(CharSequence text) {
        hasText = text;
        return this;
    }

    public AlertDialog setDismissListener(DialogDismissListener listener) {
        dismissListener = listener;
        return this;
    }

    @AfterViews
    protected void populateViews() {
        text.setMovementMethod(LinkMovementMethod.getInstance());

        if (null != positivePair) {
            if (null != positivePair.first) {
                positiveButton.setText(positivePair.first);
            }

            positiveButton.setVisibility(View.VISIBLE);
        } else {
            positiveButton.setVisibility(View.GONE);
        }

        if (null != negativePair) {
            if (null != negativePair.first) {
                negativeButton.setText(negativePair.first);
            }

            negativeButton.setVisibility(View.VISIBLE);
        } else {
            negativeButton.setVisibility(View.GONE);
        }

        if (null != hasTitle) {
            title.setText(hasTitle);
            title.setVisibility(View.VISIBLE);
        } else {
            title.setVisibility(View.GONE);
        }

        if (null != hasText) {
            text.setText(hasText);
            text.setVisibility(View.VISIBLE);
        } else {
            text.setVisibility(View.GONE);
        }

        setCancelable(false);
    }

    @Click(R.id.alert_negative_button)
    protected void negativeButtonClicked() {
        if (null != negativePair && null != negativePair.second) {
            negativePair.second.onClick(negativeButton);
        }

        getActivity().onBackPressed();
    }

    @Click(R.id.alert_positive_button)
    protected void positiveButtonClicked() {
        if (null != positivePair && null != positivePair.second) {
            positivePair.second.onClick(positiveButton);
        }
        if( isDismissOnPositiveClick() ){
            dismiss();
        } else {
            getActivity().onBackPressed();
        }

    }

    @Override
    public void dismiss() {
        super.dismiss();

        if (null != dismissListener) {
            dismissListener.onDialogDismiss();
        }
    }

    public boolean isDismissOnPositiveClick() {
        return dismissOnPositiveClick;
    }

    public AlertDialog setDismissOnPositiveClick(boolean dismissOnPositiveClick) {
        this.dismissOnPositiveClick = dismissOnPositiveClick;
        return this;
    }

    public void show(FragmentManager fragmentManager) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        transaction.add(android.R.id.content, this)
                .addToBackStack(null)
                .commitAllowingStateLoss();

        fragmentManager.executePendingTransactions();
    }

    public static AlertDialog buildDialog(Activity activity) {
        return build(activity, null);
    }

    public static AlertDialog build(Activity activity, Builder builder) {
        AlertDialog dialog = new AlertDialog_();
        if (null != builder) {
            builder.buildAlertDialog(dialog);
        }

        return dialog;
    }
}

