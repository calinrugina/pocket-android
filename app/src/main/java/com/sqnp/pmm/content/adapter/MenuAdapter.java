package com.sqnp.pmm.content.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.view.MenuItemView;
import com.sqnp.pmm.content.view.MenuItemView_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

@EBean
public class MenuAdapter extends BaseAdapter {

    @RootContext
    protected Context context;

    protected String[] options;

    @AfterInject
    protected void afterInject() {
        options = context.getResources().getStringArray(R.array.menu_options);
    }

    @Override
    public int getCount() {
        return options.length;
    }

    @Override
    public String getItem(int i) {
        return options[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MenuItemView itemView;
        if (null == view) {
            itemView = MenuItemView_.build(context);
        } else {
            itemView = (MenuItemView) view;
        }

        itemView.bind(getItem(i));
        return itemView;
    }
}
