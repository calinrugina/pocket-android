package com.sqnp.pmm.content.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.adapter.SimpleSpinnerAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.dialog.DateDialog;
import com.sqnp.pmm.content.feature.EntityExtractor;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.Reminder;
import com.sqnp.pmm.service.receiver.AlarmReceiver;
import com.sqnp.pmm.service.receiver.AlarmReceiver_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EFragment(R.layout.fragment_reminder)
public class ReminderFragment extends BaseFragment implements SectionTitleProvider {

    @ViewById(R.id.reminder_account)
    protected Spinner account;

    @ViewById(R.id.reminder_title)
    protected TextView title;

    @ViewById(R.id.reminder_amount)
    protected TextView amount;

    @ViewById(R.id.reminder_type)
    protected Spinner type;

    @ViewById(R.id.reminder_date_time)
    protected EditText dateTime;

    @Bean
    protected SimpleSpinnerAdapter<Account> accounts;

    @Bean
    protected SimpleSpinnerAdapter<Pair<Integer, String>> types;

    @AfterViews
    protected void configureViews() {
        accounts.setEntityExtractor(new EntityExtractor<Account, String>() {

            @Override
            public String extractEntity(Account entity) {
                return entity.getName();
            }
        });
        accounts.populate(((MainActivity) getActivity()).getAccountsRepository().queryForAll());
        account.setAdapter(accounts);

        List<Pair<Integer, String>> reminderTypes = new ArrayList<>();
        int i = 1;
        for (String reminderType : getResources().getStringArray(R.array.spinner_transaction_type)) {
            reminderTypes.add(new Pair<>(i++, reminderType));
        }

        types.setEntityExtractor(new EntityExtractor<Pair<Integer, String>, String>() {

            @Override
            public String extractEntity(Pair<Integer, String> entity) {
                return entity.second;
            }
        });
        types.populate(reminderTypes);
        type.setAdapter(types);
    }

    @Click(R.id.reminder_date_time)
    protected void dateTimeClicked() {
        DateDialog dialog = new DateDialog(getActivity());
        dialog.setEditText(dateTime);
        dialog.setTimePickerInterval(15);
        dialog.setUseTime(true);
        dialog.setMinDate(new Date());

        dialog.show();
    }

    @Click(R.id.reminder_save)
    protected void saveClicked() {
        AlertDialog dialog = null;
        if (TextUtils.isEmpty(title.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.reminder_alert_no_title))
                            .setPositiveButton(null, null);
                }
            });
        } else if (TextUtils.isEmpty(amount.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.reminder_alert_no_amount))
                            .setPositiveButton(null, null);
                }
            });
        } else if (TextUtils.isEmpty(dateTime.getText())) {
            dialog = AlertDialog.build(getActivity(), new AlertDialog.Builder() {

                @Override
                public AlertDialog buildAlertDialog(AlertDialog dialog) {
                    return dialog
                            .setText(getString(R.string.reminder_alert_no_date_time))
                            .setPositiveButton(null, null);
                }
            });
        }

        if (null != dialog) {
            dialog.show(getFragmentManager());
            return;
        }

        doSave();
    }

    @Background
    protected void doSave() {
        Reminder reminder = new Reminder();
        reminder.setTitle(title.getText().toString());
        reminder.setAccountId(((Account) account.getSelectedItem()).getId());
        reminder.setCurrency(((Account) account.getSelectedItem()).getCurrency());
        reminder.setCreatedAt(new Date());
        reminder.setType(((Pair<Integer, String>) type.getSelectedItem()).first);
        reminder.setValue(Double.valueOf(amount.getText().toString()));
        reminder.setNextDate((Date) dateTime.getTag());

        ((MainActivity) getActivity()).getRemindersRepository().create(reminder);
        onSaved(reminder);
    }

    @UiThread
    protected void onSaved(Reminder reminder) {
        if (null == getActivity()) {
            return;
        }

        Intent alarmIntent = new Intent(getActivity(), AlarmReceiver_.class);
        alarmIntent.setAction(AlarmReceiver.ACTION_NOTIFICATION);
        alarmIntent.putExtra("reminderId", reminder.getId());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, alarmIntent, 0);
        ((MainActivity) getActivity()).getAlarmManager().set(
                AlarmManager.RTC, ((Date) dateTime.getTag()).getTime(), pendingIntent);

        getActivity().onBackPressed();
    }

    @Override
    public Title getTitleHolder() {
        Title title = new Title(getString(R.string.reminder_fragment_title));
        title.setHidesMenu(true);

        return title;
    }
}
