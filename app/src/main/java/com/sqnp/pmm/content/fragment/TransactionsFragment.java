package com.sqnp.pmm.content.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.sqnp.pmm.R;
import com.sqnp.pmm.content.BaseFragmentActivity;
import com.sqnp.pmm.content.MainActivity;
import com.sqnp.pmm.content.ProgressDialog;
import com.sqnp.pmm.content.adapter.SimpleSpinnerAdapter;
import com.sqnp.pmm.content.adapter.TransactionsAdapter;
import com.sqnp.pmm.content.dialog.AlertDialog;
import com.sqnp.pmm.content.dialog.DateDialog;
import com.sqnp.pmm.content.feature.EntityExtractor;
import com.sqnp.pmm.content.feature.SectionTitleProvider;
import com.sqnp.pmm.content.feature.ViewPrototypeBuilder;
import com.sqnp.pmm.content.view.TransactionItemView;
import com.sqnp.pmm.entity.Account;
import com.sqnp.pmm.entity.History;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

@EFragment(R.layout.fragment_transactions)
public class TransactionsFragment extends BaseFragment
        implements SectionTitleProvider,
        TransactionItemView.RemoveClickedListener  {

    @FragmentArg
    protected Account account;

    @ViewById(R.id.transactions_filter)
    protected ViewGroup filter;

    @ViewById(R.id.transactions_filter_account)
    protected Spinner filterAccount;

    @ViewById(R.id.transactions_filter_start_date)
    protected TextView filterStartDate;

    @ViewById(R.id.transactions_filter_end_date)
    protected TextView filterEndDate;

    @ViewById(R.id.transactions_transactions_list)
    protected StickyListHeadersListView transactionsList;

    @Bean
    protected SimpleSpinnerAdapter<Account> filterAccounts;

    @Bean
    protected TransactionsAdapter transactions;

    protected Dialog progressDialog;

    @AfterViews
    protected void prepareViews() {

        transactions.setAccountsRepository(((MainActivity) getActivity()).getAccountsRepository());
        transactions.setHistoryRepository(((MainActivity) getActivity()).getHistoryRepository());
        transactions.setAccount(account);
        transactions.setViewPrototypeBuilder(new ViewPrototypeBuilder<TransactionItemView>() {

            @Override
            public TransactionItemView buildViewPrototype(TransactionItemView view) {
                view.setRemoveClickedListener(TransactionsFragment.this);
                return view;
            }
        });
        transactionsList.setAdapter(transactions);
        transactionsList.setOnScrollListener(transactions);

        filterAccounts.setEntityExtractor(new EntityExtractor<Account, String>() {

            @Override
            public String extractEntity(Account entity) {
                return entity.getName();
            }
        });
        filterAccount.setAdapter(filterAccounts);
        doLoadFilterAccounts();
    }

    @Click(R.id.transactions_filter_start_date)
    protected void filterStartDateClicked(View view) {
        filterDateClicked(view);
    }

    @Click(R.id.transactions_filter_end_date)
    protected void filterEndDateClicked(View view) {
        filterDateClicked(view);
    }

    protected void filterDateClicked(View view) {
        DateDialog dialog = new DateDialog(getActivity());
        dialog.setEditText((EditText) view);

        dialog.show();
    }

    @Click(R.id.transactions_filter_clear)
    protected void filterClearClicked() {
        transactions.reset();
        transactions.load();
        filterStartDate.setText(null);
        filterStartDate.setTag(null);
        filterEndDate.setText(null);
        filterEndDate.setTag(null);
        if (0 != filterAccounts.getCount()) {
            filterAccount.setSelection(0);
        }

        filter.setVisibility(View.GONE);
    }

    @Click(R.id.transactions_filter_apply)
    protected void filterApplyClicked() {
        transactions.reset();

        transactions.setItemsPerPage(Integer.MAX_VALUE);
        transactions.getPendingRequest().setStartDate((Date) filterStartDate.getTag());
        transactions.getPendingRequest().setEndDate((Date) filterEndDate.getTag());
        transactions.getPendingRequest().setAccount((Account) filterAccount.getSelectedItem());

        transactions.load();
        filter.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        reload();

    }
    protected void reload() {
        transactions.reset();
        transactions.load();
    }
    @Background
    protected void doLoadFilterAccounts() {
        onFilterAccountsLoaded(((MainActivity) getActivity()).getAccountsRepository().queryForAll());
    }

    @UiThread
    protected void onFilterAccountsLoaded(List<Account> accounts) {
        filterAccounts.populate(accounts);
        filterAccounts.notifyDataSetChanged();
    }
    @Override
    public Title getTitleHolder() {
        return new Title(getString(R.string.transactions_fragment_title),
                new Pair<Integer, OnMenuItemAttachedCallback>(R.drawable.ic_action_filter, new OnMenuItemAttachedCallback() {

                    @Override
                    public void onMenuItemAttached(ImageView view) {
                        if (null != account) {
                            view.setVisibility(View.GONE);
                        } else {
                            view.setVisibility(View.VISIBLE);
                            view.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    filter.setVisibility(filter.getVisibility() == View.VISIBLE
                                            ? View.GONE
                                            : View.VISIBLE);
                                }
                            });
                        }
                    }
                }),
                new Pair<Integer, OnMenuItemAttachedCallback>(R.drawable.ic_action_add, new OnMenuItemAttachedCallback() {

                    @Override
                    public void onMenuItemAttached(ImageView view) {
                        if (null == account
                                || 0 == ((MainActivity) getActivity()).getAccountsRepository().countOf()) {
                            view.setVisibility(View.GONE);
                        } else {
                            view.setVisibility(View.VISIBLE);
                            view.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {

                                    ((BaseFragmentActivity) getActivity()).pushFragment("transaction", new BaseFragmentActivity.FragmentBuilder() {

                                        @Override
                                        public void buildFragment(Bundle fragmentBundle) {
                                            fragmentBundle.putSerializable("account",account);
                                        }
                                    });
                                }
                            });
                        }
                    }
                }));
    }
    @Override
    public void onRemoveTransactionClicked(final History transaction) {
        AlertDialog.build(getActivity(), new AlertDialog.Builder() {

            @Override
            public AlertDialog buildAlertDialog(AlertDialog dialog) {
                return dialog
                        .setText(getString(
                                R.string.transactions_alert_remove,
                                TextUtils.isEmpty(transaction.getTitle())
                                        ? getResources().getString(R.string.transaction_item_default_title)
                                        : transaction.getTitle()))
                        .setPositiveButton(getString(R.string.transactions_alert_remove_positive), new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (null == getActivity()) {
                                    return;
                                }

                                progressDialog = new ProgressDialog(getActivity());
                                progressDialog.show();
                                progressDialog.setCancelable(false);

                                doRemoveTransaction(transaction);
                            }
                        })
                        .setNegativeButton(null, null);
            }
        }).show(getFragmentManager());
    }

    @Background
    protected void doRemoveTransaction(History transaction) {
        double diff = transaction.getValue() * (transaction.isOfType(History.TYPE_DEBIT) ? 1 : -1);
        int accountId = transaction.getAccountId();
        RuntimeExceptionDao<History, Integer> transactionsRepository = ((MainActivity) getActivity()).getHistoryRepository();
        RuntimeExceptionDao<Account, Integer> accountsRepository = ((MainActivity) getActivity()).getAccountsRepository();
        QueryBuilder<History, Integer> transactionsQueryBuilder = transactionsRepository.queryBuilder();
        try {
            transactionsQueryBuilder.where().eq(History.COLUMN_ACCOUNT_ID, accountId);
            transactionsQueryBuilder.where().gt(History.COLUMN_ID, transaction.getId());
            for (History entry: transactionsRepository.query(transactionsQueryBuilder.prepare())) {
                entry.setBalance(entry.getBalance() + diff);
                transactionsRepository.update(entry);
            }
            transactionsRepository.delete(transaction);
        } catch (SQLException e) {
            e.printStackTrace();
            onTransactionRemoved();
            return;
        }
        account = accountsRepository.queryForId(accountId);
        account.setMoney(account.getMoney() + diff);
        accountsRepository.update(account);
        onTransactionRemoved();
    }

    @UiThread
    protected void onTransactionRemoved() {
        if (null != getActivity() && null != progressDialog) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        reload();
    }
}
