package com.sqnp.pmm.content.adapter;

import android.widget.AbsListView;
import android.widget.BaseAdapter;

import com.sqnp.pmm.content.feature.BackgroundObserver;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.UiThread;

import java.util.List;

@EBean
abstract class AbstractProgressiveLoaderAdapter<E> extends BaseAdapter implements
        AbsListView.OnScrollListener {

    public static interface CustomRequestProvider {

        public Request getPendingRequest(Request request);
    }

    public static class Request {

        protected int offset;
        protected int limit;

        public int getOffset() {
            return offset;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public int getLimit() {
            return limit;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }
    }

    public static class Response<T> {

        protected List<T> data;
        protected long totalCount;

        public List<T> getData() {
            return data;
        }

        public void setData(List<T> data) {
            this.data = data;
        }

        public long getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(long totalCount) {
            this.totalCount = totalCount;
        }
    }

    protected long totalCount = 0;
    protected int visibleThreshold = 5;
    protected int currentPage = 0;
    protected int prevCount = 0;
    protected int itemsPerPage;
    protected int defaultItemsPerPage = 50;
    protected List<E> data;

    protected boolean loading = true;

    protected boolean firstTime = true;


    protected BackgroundObserver observer;

    public void setObserver(BackgroundObserver observer) {
        this.observer = observer;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public void setDefaultItemsPerPage(int defaultItemsPerPage) {
        this.defaultItemsPerPage = defaultItemsPerPage;
    }

    public void load(){
        load(false);
    }
    public void load(boolean forceClearData) {
        if (firstTime) {
            if(null != observer){
                observer.onBackgroundWillStart();
            }
        }


        doLoad(forceClearData);
    }

    public boolean hasData() {
        return null != data;
    }

    public void reset() {
        totalCount = 0;
        currentPage = 0;
        prevCount = 0;

        data = null;

        firstTime = true;
        itemsPerPage = defaultItemsPerPage;
    }

    @Background
    protected void doLoad(boolean forceClearData) {
        if (forceClearData) {
            reset();
        }

        Request request = this instanceof CustomRequestProvider
                ? ((CustomRequestProvider) this).getPendingRequest(new Request())
                : new Request();
        request.setOffset(itemsPerPage * currentPage++);
        request.setLimit(itemsPerPage);

        Response<E> response = loadData(request);

        if (null != response) {
            totalCount = response.getTotalCount();
            onLoaded(response.getData());
        } else {
            onLoaded(null);
        }
    }

    @UiThread
    protected void onLoaded(List<E> data) {
        if (null == this.data) {
            this.data = data;
        } else {
            this.data.addAll(data);
        }

        if (firstTime) {
            if(null != observer){
                observer.onBackgroundEnded();
            }

            firstTime = false;
        }

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return null == data ? 0 : data.size();
    }

    @Override
    public E getItem(int i) {
        try {
            return data.get(i);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (Integer.MAX_VALUE == itemsPerPage) {
            return;
        }

        if (prevCount < totalCount) {
            if (loading) {
                if (totalItemCount > prevCount) {
                    loading = false;
                    prevCount = totalItemCount;
                }
            }

            if (! loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                load();
                loading = true;
            }
        }
    }

    public abstract Response<E> loadData(Request request);
}