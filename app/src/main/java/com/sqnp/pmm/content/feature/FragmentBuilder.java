package com.sqnp.pmm.content.feature;

import android.os.Bundle;

public interface FragmentBuilder {

    public void buildFragment(Bundle fragmentBundle);
}
