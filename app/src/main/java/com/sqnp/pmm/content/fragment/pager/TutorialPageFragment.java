package com.sqnp.pmm.content.fragment.pager;

import android.support.v4.app.Fragment;
import android.view.View;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.feature.BindableViewHolder;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_tutorial_page)
public class TutorialPageFragment extends Fragment implements BindableViewHolder<String> {

    @ViewById(R.id.tutorial_page_container)
    protected View container;

    protected String backgroundResource;

    @AfterViews
    protected void configureViews() {
        container.setBackgroundResource(getResources().getIdentifier(
                backgroundResource,
                "drawable",
                getActivity().getPackageName()));
    }

    @Override
    public void bind(String entity) {
        backgroundResource = entity;
    }
}
