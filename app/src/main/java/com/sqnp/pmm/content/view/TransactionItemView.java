package com.sqnp.pmm.content.view;

import android.content.Context;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.dialog.DateDialog;
import com.sqnp.pmm.content.feature.BindableViewHolder;
import com.sqnp.pmm.entity.History;
import com.sqnp.pmm.support.Utils;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.transaction_item)
public class TransactionItemView extends LinearLayout implements
        BindableViewHolder<History> {

    public static interface RemoveClickedListener {

        public void onRemoveTransactionClicked(History transaction);
    }

    @ViewById(R.id.transaction_item_title)
    protected TextView title;

    @ViewById(R.id.transaction_item_date)
    protected TextView date;

    @ViewById(R.id.transaction_item_value)
    protected TextView value;

    @ViewById(R.id.transaction_item_balance)
    protected TextView balance;

    protected RemoveClickedListener removeClickedListener;

    public TransactionItemView(Context context) {
        super(context);
    }

    public void setRemoveClickedListener(RemoveClickedListener removeClickedListener) {
        this.removeClickedListener = removeClickedListener;
    }

    @Override
    public void bind(History entity) {
        setTag(entity);

        title.setText(TextUtils.isEmpty(entity.getTitle())
                ? getResources().getString(R.string.transaction_item_default_title)
                : entity.getTitle());
        if (null == entity.getCreatedAt()) {
            date.setText(null);
        } else {
            date.setText(DateDialog.DATE_FORMAT.format(entity.getCreatedAt()));
        }
        value.setText(Utils.formatBalance(
                entity.getValue() * (entity.isOfType(History.TYPE_DEBIT) ? -1 : 1),
                entity.getCurrency()));
        if (null == entity.getBalance()) {
            balance.setText(null);
        } else {
            balance.setText(Utils.formatBalance(entity.getBalance(), entity.getCurrency()));
        }
    }

    @Click(R.id.transaction_item_remove)
    protected void removeClicked() {
        if (null != removeClickedListener) {
            removeClickedListener.onRemoveTransactionClicked((History) getTag());
        }
    }
}
