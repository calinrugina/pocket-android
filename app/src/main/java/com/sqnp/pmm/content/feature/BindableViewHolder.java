package com.sqnp.pmm.content.feature;

public interface BindableViewHolder<T> {

    public void bind(T entity);
}
