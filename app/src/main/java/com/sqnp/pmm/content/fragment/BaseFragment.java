package com.sqnp.pmm.content.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.sqnp.pmm.content.MainActivity;

public class BaseFragment extends Fragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        registerWithFeatures();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerWithFeatures();
    }

    protected void registerWithFeatures() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).registerFragmentWithFeatures(this);
        }
    }

    protected void requestNoAds() {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).hideAdFragmentContainer();
        }
    }
}
