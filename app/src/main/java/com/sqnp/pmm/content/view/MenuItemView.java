package com.sqnp.pmm.content.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sqnp.pmm.R;
import com.sqnp.pmm.content.feature.BindableViewHolder;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.menu_item)
public class MenuItemView extends LinearLayout implements BindableViewHolder<String> {

    @ViewById(R.id.menu_item_title)
    protected TextView title;

    public MenuItemView(Context context) {
        super(context);
    }

    @Override
    public void bind(String entity) {
        title.setText(getResources().getString(getResources().getIdentifier(
                String.format("menu_item_%s", entity),
                "string",
                getContext().getPackageName())));
        title.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(
                        getResources().getIdentifier(
                                String.format("ic_menu_item_%s_selectable", entity),
                                "drawable",
                                getContext().getPackageName())),
                null,
                null,
                null);
    }
}
