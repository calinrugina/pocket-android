//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.2.
//


package com.sqnp.pmm.content.adapter;

import java.util.List;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.sqnp.pmm.entity.History;
import org.androidannotations.api.BackgroundExecutor;

public final class TransactionsAdapter_
    extends TransactionsAdapter
{

    private Context context_;
    private Handler handler_ = new Handler(Looper.getMainLooper());

    private TransactionsAdapter_(Context context) {
        context_ = context;
        init_();
    }

    public static TransactionsAdapter_ getInstance_(Context context) {
        return new TransactionsAdapter_(context);
    }

    private void init_() {
    }

    public void rebind(Context context) {
        context_ = context;
        init_();
    }

    @Override
    public void onLoaded(final List<History> data) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                TransactionsAdapter_.super.onLoaded(data);
            }

        }
        );
    }

    @Override
    public void onMissingAccountsLoaded(final List<History> data) {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                TransactionsAdapter_.super.onMissingAccountsLoaded(data);
            }

        }
        );
    }

    @Override
    public void doLoadMissingAccounts(final List<History> data) {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    TransactionsAdapter_.super.doLoadMissingAccounts(data);
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

    @Override
    public void doLoad(final boolean forceClearData) {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    TransactionsAdapter_.super.doLoad(forceClearData);
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

}
