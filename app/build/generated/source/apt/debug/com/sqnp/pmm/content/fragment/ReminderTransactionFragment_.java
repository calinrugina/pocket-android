//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.2.
//


package com.sqnp.pmm.content.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import com.sqnp.pmm.R.layout;
import com.sqnp.pmm.content.adapter.SimpleSpinnerAdapter_;
import com.sqnp.pmm.entity.Reminder;
import org.androidannotations.api.BackgroundExecutor;
import org.androidannotations.api.builder.FragmentBuilder;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

public final class ReminderTransactionFragment_
    extends com.sqnp.pmm.content.fragment.ReminderTransactionFragment
    implements HasViews, OnViewChangedListener
{

    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();
    private View contentView_;
    public final static String REMINDER_ARG = "reminder";
    private Handler handler_ = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    @Override
    public View findViewById(int id) {
        if (contentView_ == null) {
            return null;
        }
        return contentView_.findViewById(id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView_ = super.onCreateView(inflater, container, savedInstanceState);
        if (contentView_ == null) {
            contentView_ = inflater.inflate(layout.fragment_reminder_transaction, container, false);
        }
        return contentView_;
    }

    @Override
    public void onDestroyView() {
        contentView_ = null;
        super.onDestroyView();
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        injectFragmentArguments_();
        types = SimpleSpinnerAdapter_.getInstance_(getActivity());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static ReminderTransactionFragment_.FragmentBuilder_ builder() {
        return new ReminderTransactionFragment_.FragmentBuilder_();
    }

    @Override
    public void onViewChanged(HasViews hasViews) {
        fragmentTitle = ((TextView) hasViews.findViewById(com.sqnp.pmm.R.id.reminder_transaction_fragment_title));
        title = ((TextView) hasViews.findViewById(com.sqnp.pmm.R.id.reminder_transaction_title));
        type = ((Spinner) hasViews.findViewById(com.sqnp.pmm.R.id.reminder_transaction_type));
        amount = ((TextView) hasViews.findViewById(com.sqnp.pmm.R.id.reminder_transaction_amount));
        {
            View view = hasViews.findViewById(com.sqnp.pmm.R.id.reminder_transaction_save);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View view) {
                        ReminderTransactionFragment_.this.saveClicked();
                    }

                }
                );
            }
        }
        configureViews();
    }

    private void injectFragmentArguments_() {
        Bundle args_ = getArguments();
        if (args_!= null) {
            if (args_.containsKey(REMINDER_ARG)) {
                reminder = ((Reminder) args_.getSerializable(REMINDER_ARG));
            }
        }
    }

    @Override
    public void onSaved() {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                ReminderTransactionFragment_.super.onSaved();
            }

        }
        );
    }

    @Override
    public void onAccountLoaded() {
        handler_.post(new Runnable() {


            @Override
            public void run() {
                ReminderTransactionFragment_.super.onAccountLoaded();
            }

        }
        );
    }

    @Override
    public void doSave() {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    ReminderTransactionFragment_.super.doSave();
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

    @Override
    public void doLoadAccount() {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0, "") {


            @Override
            public void execute() {
                try {
                    ReminderTransactionFragment_.super.doLoadAccount();
                } catch (Throwable e) {
                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }

        }
        );
    }

    public static class FragmentBuilder_
        extends FragmentBuilder<ReminderTransactionFragment_.FragmentBuilder_, com.sqnp.pmm.content.fragment.ReminderTransactionFragment>
    {


        @Override
        public com.sqnp.pmm.content.fragment.ReminderTransactionFragment build() {
            ReminderTransactionFragment_ fragment_ = new ReminderTransactionFragment_();
            fragment_.setArguments(args);
            return fragment_;
        }

        public ReminderTransactionFragment_.FragmentBuilder_ reminder(Reminder reminder) {
            args.putSerializable(REMINDER_ARG, reminder);
            return this;
        }

    }

}
