//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.2.
//


package com.sqnp.pmm.content.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.sqnp.pmm.R.layout;
import com.sqnp.pmm.content.adapter.RemindersAdapter_;
import com.sqnp.pmm.entity.Account;
import org.androidannotations.api.builder.FragmentBuilder;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public final class RemindersFragment_
    extends com.sqnp.pmm.content.fragment.RemindersFragment
    implements HasViews, OnViewChangedListener
{

    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();
    private View contentView_;
    public final static String ACCOUNT_ARG = "account";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
    }

    @Override
    public View findViewById(int id) {
        if (contentView_ == null) {
            return null;
        }
        return contentView_.findViewById(id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        contentView_ = super.onCreateView(inflater, container, savedInstanceState);
        if (contentView_ == null) {
            contentView_ = inflater.inflate(layout.fragment_reminders, container, false);
        }
        return contentView_;
    }

    @Override
    public void onDestroyView() {
        contentView_ = null;
        super.onDestroyView();
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
        injectFragmentArguments_();
        reminders = RemindersAdapter_.getInstance_(getActivity());
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static RemindersFragment_.FragmentBuilder_ builder() {
        return new RemindersFragment_.FragmentBuilder_();
    }

    @Override
    public void onViewChanged(HasViews hasViews) {
        remindersList = ((StickyListHeadersListView) hasViews.findViewById(com.sqnp.pmm.R.id.reminders_reminders_list));
        configureViews();
    }

    private void injectFragmentArguments_() {
        Bundle args_ = getArguments();
        if (args_!= null) {
            if (args_.containsKey(ACCOUNT_ARG)) {
                account = ((Account) args_.getSerializable(ACCOUNT_ARG));
            }
        }
    }

    public static class FragmentBuilder_
        extends FragmentBuilder<RemindersFragment_.FragmentBuilder_, com.sqnp.pmm.content.fragment.RemindersFragment>
    {


        @Override
        public com.sqnp.pmm.content.fragment.RemindersFragment build() {
            RemindersFragment_ fragment_ = new RemindersFragment_();
            fragment_.setArguments(args);
            return fragment_;
        }

        public RemindersFragment_.FragmentBuilder_ account(Account account) {
            args.putSerializable(ACCOUNT_ARG, account);
            return this;
        }

    }

}
